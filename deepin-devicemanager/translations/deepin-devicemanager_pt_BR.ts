<?xml version="1.0" ?><!DOCTYPE TS><TS language="pt_BR" version="2.1">
<context>
    <name>CmdButtonWidget</name>
    <message>
        <location filename="../src/Widget/CmdButtonWidget.cpp" line="14"/>
        <source>More</source>
        <translation>Mais</translation>
    </message>
</context>
<context>
    <name>DetailButton</name>
    <message>
        <location filename="../src/Page/PageDetail.cpp" line="33"/>
        <location filename="../src/Page/PageDetail.cpp" line="36"/>
        <source>More</source>
        <translation>Mais</translation>
    </message>
    <message>
        <location filename="../src/Page/PageDetail.cpp" line="34"/>
        <source>Collapse</source>
        <translation>Recolher</translation>
    </message>
</context>
<context>
    <name>DetailTreeView</name>
    <message>
        <location filename="../src/Widget/DetailTreeView.cpp" line="83"/>
        <location filename="../src/Widget/DetailTreeView.cpp" line="249"/>
        <source>More</source>
        <translation>Mais</translation>
    </message>
    <message>
        <location filename="../src/Widget/DetailTreeView.cpp" line="255"/>
        <source>Collapse</source>
        <translation>Recolher</translation>
    </message>
</context>
<context>
    <name>DetailViewDelegate</name>
    <message>
        <location filename="../src/Widget/DetailViewDelegate.cpp" line="226"/>
        <location filename="../src/Widget/DetailViewDelegate.cpp" line="230"/>
        <source>Disable</source>
        <translation>Desativar</translation>
    </message>
</context>
<context>
    <name>DeviceAudio</name>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="221"/>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="299"/>
        <source>Disable</source>
        <translation>Desativar</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="237"/>
        <source>Device Name</source>
        <translation>Nome do Dispositivo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="266"/>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="290"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="267"/>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="291"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="268"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="269"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="270"/>
        <source>Bus Info</source>
        <translation>Informações do Barramento</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="276"/>
        <source>Chip</source>
        <translation>Chip</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="277"/>
        <source>Capabilities</source>
        <translation>Recursos</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="278"/>
        <source>Clock</source>
        <translation>Frequência</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="279"/>
        <source>Width</source>
        <translation>Largura</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="280"/>
        <source>Memory</source>
        <translation>Memória</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="281"/>
        <source>IRQ</source>
        <translation>IRQ</translation>
    </message>
</context>
<context>
    <name>DeviceBaseInfo</name>
    <message>
        <location filename="../src/DeviceManager/DeviceInfo.cpp" line="441"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInfo.cpp" line="442"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInfo.cpp" line="443"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
</context>
<context>
    <name>DeviceBios</name>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="231"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="232"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="233"/>
        <source>Chipset</source>
        <translation>Chipset</translation>
    </message>
</context>
<context>
    <name>DeviceBluetooth</name>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="216"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="217"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="218"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="219"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="244"/>
        <source>Speed</source>
        <translation>Velocidade</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="245"/>
        <source>Maximum Power</source>
        <translation>Potência Máxima</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="246"/>
        <source>Driver Version</source>
        <translation>Versão do Driver</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="247"/>
        <source>Driver</source>
        <translation>Driver</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="248"/>
        <source>Capabilities</source>
        <translation>Recursos</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="249"/>
        <source>Bus Info</source>
        <translation>Informações do Barramento</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="250"/>
        <source>Logical Name</source>
        <translation>Nome Lógico</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="251"/>
        <source>MAC Address</source>
        <translation>Endereço MAC</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="262"/>
        <source>Disable</source>
        <translation>Desativar</translation>
    </message>
</context>
<context>
    <name>DeviceCdrom</name>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="153"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="154"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="155"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="156"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="157"/>
        <source>Bus Info</source>
        <translation>Informações do Barramento</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="158"/>
        <source>Capabilities</source>
        <translation>Recursos</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="159"/>
        <source>Driver</source>
        <translation>Driver</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="160"/>
        <source>Maximum Power</source>
        <translation>Potência Máxima</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="161"/>
        <source>Speed</source>
        <translation>Velocidade</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="175"/>
        <source>Disable</source>
        <translation>Desativar</translation>
    </message>
</context>
<context>
    <name>DeviceComputer</name>
    <message>
        <location filename="../src/DeviceManager/DeviceComputer.cpp" line="165"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
</context>
<context>
    <name>DeviceCpu</name>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="44"/>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="337"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="45"/>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="338"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="46"/>
        <source>CPU ID</source>
        <translation>ID da CPU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="47"/>
        <source>Core ID</source>
        <translation>ID do Núcleo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="48"/>
        <source>Threads</source>
        <translation>Threads</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="49"/>
        <source>Current Speed</source>
        <translation>Velocidade Atual</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="50"/>
        <source>BogoMIPS</source>
        <translation>BogoMIPS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="51"/>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="340"/>
        <source>Architecture</source>
        <translation>Arquitetura</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="52"/>
        <source>CPU Family</source>
        <translation>Família da CPU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="53"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="185"/>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="197"/>
        <source>Processor</source>
        <translation>Processador</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="195"/>
        <source>Core(s)</source>
        <translation>Núcleo(s)</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="321"/>
        <source>Virtualization</source>
        <translation>Virtualização</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="322"/>
        <source>Flags</source>
        <translation>Bandeiras</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="323"/>
        <source>Extensions</source>
        <translation>Extensões</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="324"/>
        <source>L3 Cache</source>
        <translation>Cache L3</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="325"/>
        <source>L2 Cache</source>
        <translation>Cache L2</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="326"/>
        <source>L1i Cache</source>
        <translation>Cache L1i</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="327"/>
        <source>L1d Cache</source>
        <translation>Cache L1d</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="328"/>
        <source>Stepping</source>
        <translation>Pisar</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="339"/>
        <source>Speed</source>
        <translation>Velocidade</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="339"/>
        <source>Max Speed</source>
        <translation>Velocidade Máxima</translation>
    </message>
</context>
<context>
    <name>DeviceGpu</name>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="55"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="56"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="57"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="58"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="59"/>
        <source>Graphics Memory</source>
        <translation>Memória Gráfica</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="313"/>
        <source>Physical ID</source>
        <translation>ID Físico</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="314"/>
        <source>Memory</source>
        <translation>Memória</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="315"/>
        <source>IO Port</source>
        <translation>Porta de E/S</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="316"/>
        <source>Bus Info</source>
        <translation>Informações do Barramento</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="317"/>
        <source>Maximum Resolution</source>
        <translation>Resolução Máxima</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="318"/>
        <source>Minimum Resolution</source>
        <translation>Resolução Mínima</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="319"/>
        <source>Current Resolution</source>
        <translation>Resolução Atual</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="320"/>
        <source>Driver</source>
        <translation>Driver</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="321"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="322"/>
        <source>Clock</source>
        <translation>Frequência</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="323"/>
        <source>DP</source>
        <translation>DP</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="324"/>
        <source>eDP</source>
        <translation>eDP</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="325"/>
        <source>HDMI</source>
        <translation>HDMI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="326"/>
        <source>VGA</source>
        <translation>VGA</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="327"/>
        <source>DVI</source>
        <translation>DVI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="328"/>
        <source>Display Output</source>
        <translation>Exibir Saída</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="329"/>
        <source>Capabilities</source>
        <translation>Recursos</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="330"/>
        <source>IRQ</source>
        <translation>IRQ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="331"/>
        <source>Width</source>
        <translation>Largura</translation>
    </message>
</context>
<context>
    <name>DeviceImage</name>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="148"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="149"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="150"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="151"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="152"/>
        <source>Bus Info</source>
        <translation>Informações do Barramento</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="158"/>
        <source>Speed</source>
        <translation>Velocidade</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="159"/>
        <source>Maximum Power</source>
        <translation>Potência Máxima</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="160"/>
        <source>Driver</source>
        <translation>Driver</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="161"/>
        <source>Capabilities</source>
        <translation>Recursos</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="172"/>
        <source>Disable</source>
        <translation>Desativar</translation>
    </message>
</context>
<context>
    <name>DeviceInput</name>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="211"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="212"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="213"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="214"/>
        <source>Interface</source>
        <translation>Interface</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="215"/>
        <source>Bus Info</source>
        <translation>Informações do Barramento</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="221"/>
        <source>Speed</source>
        <translation>Velocidade</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="222"/>
        <source>Maximum Power</source>
        <translation>Potência Máxima</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="223"/>
        <source>Driver</source>
        <translation>Driver</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="224"/>
        <source>Capabilities</source>
        <translation>Recursos</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="225"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="235"/>
        <source>Disable</source>
        <translation>Desativar</translation>
    </message>
</context>
<context>
    <name>DeviceManager</name>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="136"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="190"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="820"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="839"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="859"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="867"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="882"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="892"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="907"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="918"/>
        <source>Overview</source>
        <translation>Visão Geral</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="138"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="169"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="991"/>
        <source>CPU</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="141"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="170"/>
        <source>Motherboard</source>
        <translation>Placa-mãe</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="142"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="171"/>
        <source>Memory</source>
        <translation>Memória</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="143"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="172"/>
        <source>Display Adapter</source>
        <translation>Adaptador de Vídeo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="144"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="173"/>
        <source>Sound Adapter</source>
        <translation>Adaptador de Som</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="145"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="174"/>
        <source>Storage</source>
        <translation>Armazenamento</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="146"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="175"/>
        <source>Other PCI Devices</source>
        <translation>Outros Dispositivos PCI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="147"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="176"/>
        <source>Battery</source>
        <translation>Bateria</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="150"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="177"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="151"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="178"/>
        <source>Network Adapter</source>
        <translation>Adaptador de Rede</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="154"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="179"/>
        <source>Mouse</source>
        <translation>Mouse</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="155"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="180"/>
        <source>Keyboard</source>
        <translation>Teclado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="158"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="181"/>
        <source>Monitor</source>
        <translation>Monitor</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="159"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="182"/>
        <source>CD-ROM</source>
        <translation>CD-ROM</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="160"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="183"/>
        <source>Printer</source>
        <translation>Impressora</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="161"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="184"/>
        <source>Camera</source>
        <translation>Câmera</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="162"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="185"/>
        <source>Other Devices</source>
        <comment>Other Input Devices</comment>
        <translation>Outros Dispositivos</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="825"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="885"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="912"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="832"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="888"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="914"/>
        <source>OS</source>
        <translation>SO</translation>
    </message>
</context>
<context>
    <name>DeviceMemory</name>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="89"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="115"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="90"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="116"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="91"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="119"/>
        <source>Size</source>
        <translation>Tamanho</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="92"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="117"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="93"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="118"/>
        <source>Speed</source>
        <translation>Velocidade</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="94"/>
        <source>Total Width</source>
        <translation>Largura Total</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="95"/>
        <source>Locator</source>
        <translation>Localizador</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="96"/>
        <source>Serial Number</source>
        <translation>Número de Série</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="103"/>
        <source>Configured Voltage</source>
        <translation>Tensão Configurada</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="104"/>
        <source>Maximum Voltage</source>
        <translation>Tensão Máxima</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="105"/>
        <source>Minimum Voltage</source>
        <translation>Tensão Mínima</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="106"/>
        <source>Configured Speed</source>
        <translation>Velocidade Configurada</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="107"/>
        <source>Data Width</source>
        <translation>Largura de Dados</translation>
    </message>
</context>
<context>
    <name>DeviceMonitor</name>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="266"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="267"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="268"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="269"/>
        <source>Display Input</source>
        <translation>Exibir Entrada</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="270"/>
        <source>Interface Type</source>
        <translation>Tipo de Interface</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="276"/>
        <source>Support Resolution</source>
        <translation>Resolução de Suporte</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="277"/>
        <source>Current Resolution</source>
        <translation>Resolução Atual</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="278"/>
        <source>Primary Monitor</source>
        <translation>Monitor Principal</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="279"/>
        <source>Display Ratio</source>
        <translation>Formato</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="280"/>
        <source>Size</source>
        <translation>Tamanho</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="281"/>
        <source>Serial Number</source>
        <translation>Número de Série</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="282"/>
        <source>Product Date</source>
        <translation>Data do Produto</translation>
    </message>
</context>
<context>
    <name>DeviceNetwork</name>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="235"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="236"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="237"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="238"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="239"/>
        <source>Bus Info</source>
        <translation>Informações do Barramento</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="240"/>
        <source>Capabilities</source>
        <translation>Recursos</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="241"/>
        <source>Driver</source>
        <translation>Driver</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="242"/>
        <source>Driver Version</source>
        <translation>Versão do Driver</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="248"/>
        <source>Capacity</source>
        <translation>Capacidade</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="249"/>
        <source>Speed</source>
        <translation>Velocidade</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="250"/>
        <source>Port</source>
        <translation>Porta</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="251"/>
        <source>Multicast</source>
        <translation>Multicast</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="252"/>
        <source>Link</source>
        <translation>Link</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="253"/>
        <source>Latency</source>
        <translation>Latência</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="254"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="255"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="256"/>
        <source>Duplex</source>
        <translation>Duplex</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="257"/>
        <source>Broadcast</source>
        <translation>Transmissão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="258"/>
        <source>Auto Negotiation</source>
        <translation>Negociação Automática</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="259"/>
        <source>Clock</source>
        <translation>Frequência</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="260"/>
        <source>Width</source>
        <translation>Largura</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="261"/>
        <source>Memory</source>
        <translation>Memória</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="262"/>
        <source>IRQ</source>
        <translation>IRQ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="263"/>
        <source>MAC Address</source>
        <translation>Endereço MAC</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="264"/>
        <source>Logical Name</source>
        <translation>Nome Lógico</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="274"/>
        <source>Disable</source>
        <translation>Desativar</translation>
    </message>
</context>
<context>
    <name>DeviceOtherPCI</name>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="111"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="112"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="113"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="114"/>
        <source>Bus Info</source>
        <translation>Informações do Barramento</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="115"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="121"/>
        <source>Input/Output</source>
        <translation>Entrada/Saída</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="122"/>
        <source>Memory</source>
        <translation>Memória</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="123"/>
        <source>IRQ</source>
        <translation>IRQ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="124"/>
        <source>Latency</source>
        <translation>Latência</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="125"/>
        <source>Clock</source>
        <translation>Frequência</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="126"/>
        <source>Width</source>
        <translation>Largura</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="127"/>
        <source>Driver</source>
        <translation>Driver</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="128"/>
        <source>Capabilities</source>
        <translation>Recursos</translation>
    </message>
</context>
<context>
    <name>DeviceOthers</name>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="130"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="131"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="132"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="133"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="134"/>
        <source>Bus Info</source>
        <translation>Informações do Barramento</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="135"/>
        <source>Capabilities</source>
        <translation>Recursos</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="136"/>
        <source>Driver</source>
        <translation>Driver</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="137"/>
        <source>Maximum Power</source>
        <translation>Potência Máxima</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="138"/>
        <source>Speed</source>
        <translation>Velocidade</translation>
    </message>
</context>
<context>
    <name>DevicePower</name>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="210"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="211"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="212"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="213"/>
        <source>Serial Number</source>
        <translation>Número de Série</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="214"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="215"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="216"/>
        <source>Capacity</source>
        <translation>Capacidade</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="217"/>
        <source>Voltage</source>
        <translation>Tensão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="218"/>
        <source>Slot</source>
        <translation>Slot</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="219"/>
        <source>Design Capacity</source>
        <translation>Capacidade de Projeto</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="220"/>
        <source>Design Voltage</source>
        <translation>Tensão de Projeto</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="221"/>
        <source>SBDS Version</source>
        <translation>Versão do SBDS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="222"/>
        <source>SBDS Serial Number</source>
        <translation>Número de Série do SBDS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="223"/>
        <source>SBDS Manufacture Date</source>
        <translation>Data de Fabricação do SBDS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="224"/>
        <source>SBDS Chemistry</source>
        <translation>Química do SBDS</translation>
    </message>
</context>
<context>
    <name>DevicePrint</name>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="148"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="149"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="150"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="151"/>
        <source>Serial Number</source>
        <translation>Número de Série</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="157"/>
        <source>Shared</source>
        <translation>Compartilhado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="158"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="159"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="160"/>
        <source>Interface Type</source>
        <translation>Tipo de Interface</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="170"/>
        <source>Disable</source>
        <translation>Desativar</translation>
    </message>
</context>
<context>
    <name>DeviceStorage</name>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="390"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="416"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="391"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="417"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="392"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="418"/>
        <source>Media Type</source>
        <translation>Tipo de Mídia</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="393"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="419"/>
        <source>Size</source>
        <translation>Tamanho</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="394"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="395"/>
        <source>Capabilities</source>
        <translation>Recursos</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="401"/>
        <source>Power Cycle Count</source>
        <translation>Contagem de Ciclos de Energia</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="402"/>
        <source>Power On Hours</source>
        <translation>Horas Ligado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="403"/>
        <source>Firmware Version</source>
        <translation>Versão do Firmware</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="404"/>
        <source>Speed</source>
        <translation>Velocidade</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="405"/>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="406"/>
        <source>Serial Number</source>
        <translation>Número de Série</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="407"/>
        <source>Interface</source>
        <translation>Interface</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="408"/>
        <source>Rotation Rate</source>
        <translation>Taxa de Rotação</translation>
    </message>
</context>
<context>
    <name>LogTreeView</name>
    <message>
        <location filename="../src/Widget/logtreeview.cpp" line="82"/>
        <location filename="../src/Widget/logtreeview.cpp" line="101"/>
        <location filename="../src/Widget/logtreeview.cpp" line="103"/>
        <source>Disable</source>
        <translation>Desativar</translation>
    </message>
</context>
<context>
    <name>LogViewItemDelegate</name>
    <message>
        <location filename="../src/Widget/logviewitemdelegate.cpp" line="158"/>
        <source>Disable</source>
        <translation>Desativar</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="101"/>
        <source>Device Info</source>
        <comment>export file's name</comment>
        <translation>Informações do Dispositivo</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="174"/>
        <source>Display shortcuts</source>
        <translation>Exibir atalhos</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="175"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="176"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="177"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="181"/>
        <source>System</source>
        <translation>Sistema</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="188"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="189"/>
        <source>Refresh</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="193"/>
        <source>Device Manager</source>
        <translation>Gerenciador de Dispositivos</translation>
    </message>
</context>
<context>
    <name>PageDetail</name>
    <message>
        <location filename="../src/Page/PageDetail.cpp" line="284"/>
        <source>More</source>
        <translation>Mais</translation>
    </message>
</context>
<context>
    <name>PageListView</name>
    <message>
        <location filename="../src/Page/PageListView.cpp" line="14"/>
        <source>Refresh</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <location filename="../src/Page/PageListView.cpp" line="15"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../src/Page/PageListView.cpp" line="17"/>
        <source>Overview</source>
        <translation>Visão Geral</translation>
    </message>
</context>
<context>
    <name>PageMultiInfo</name>
    <message>
        <location filename="../src/Page/PageMultiInfo.cpp" line="95"/>
        <source>Failed to enable the device</source>
        <translation>Falha ao ativar o dispositivo</translation>
    </message>
    <message>
        <location filename="../src/Page/PageMultiInfo.cpp" line="97"/>
        <source>Failed to disable the device</source>
        <translation>Falha ao desativar o dispositivo</translation>
    </message>
</context>
<context>
    <name>PageOverview</name>
    <message>
        <location filename="../src/Page/PageOverview.cpp" line="27"/>
        <source>Refresh</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <location filename="../src/Page/PageOverview.cpp" line="28"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../src/Page/PageOverview.cpp" line="29"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../src/Page/PageOverview.cpp" line="61"/>
        <source>Overview</source>
        <translation>Visão Geral</translation>
    </message>
</context>
<context>
    <name>PageSingleInfo</name>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="21"/>
        <source>Refresh</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="22"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="23"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="24"/>
        <location filename="../src/Page/PageSingleInfo.cpp" line="77"/>
        <location filename="../src/Page/PageSingleInfo.cpp" line="120"/>
        <location filename="../src/Page/PageSingleInfo.cpp" line="153"/>
        <source>Enable</source>
        <translation>Ativar</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="118"/>
        <location filename="../src/Page/PageSingleInfo.cpp" line="168"/>
        <source>Disable</source>
        <translation>Desativar</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="159"/>
        <source>Failed to disable the device</source>
        <translation>Falha ao desativar o dispositivo</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="174"/>
        <source>Failed to enable the device</source>
        <translation>Falha ao ativar o dispositivo</translation>
    </message>
</context>
<context>
    <name>PageTableHeader</name>
    <message>
        <location filename="../src/Page/PageTableHeader.cpp" line="61"/>
        <source>Disable</source>
        <translation>Desativar</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="238"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="36"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="384"/>
        <source>SubVendor</source>
        <translation>Subfabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="239"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="37"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="383"/>
        <source>SubDevice</source>
        <translation>Subdispositivo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="240"/>
        <source>Driver</source>
        <translation>Driver</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="241"/>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="135"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="38"/>
        <source>Driver Modules</source>
        <translation>Módulos do Driver</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="242"/>
        <source>Driver Status</source>
        <translation>Status do Driver</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="243"/>
        <source>Driver Activation Cmd</source>
        <translation>Cmd de Ativação do Driver</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="244"/>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="140"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="39"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="380"/>
        <source>Config Status</source>
        <translation>Status da Configuração</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="247"/>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="203"/>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="142"/>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="205"/>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="228"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="377"/>
        <source>physical id</source>
        <translation>id físico</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="248"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="40"/>
        <source>latency</source>
        <translation>latência</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="250"/>
        <source>Phys</source>
        <translation>Phys</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="251"/>
        <source>Sysfs</source>
        <translation>Sysfs</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="252"/>
        <source>Handlers</source>
        <translation>Controladores</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="253"/>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="198"/>
        <source>PROP</source>
        <translation>PROP</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="254"/>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="199"/>
        <source>EV</source>
        <translation>EV</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="255"/>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="200"/>
        <source>KEY</source>
        <translation>KEY</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="257"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="258"/>
        <source>Vendor</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="259"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="260"/>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="182"/>
        <source>Bus</source>
        <translation>Barramento</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="25"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="215"/>
        <source>BIOS Information</source>
        <translation>Informações da BIOS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="47"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="216"/>
        <source>Base Board Information</source>
        <translation>Informações da Placa de Base</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="69"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="217"/>
        <source>System Information</source>
        <translation>Informações do Sistema</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="85"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="218"/>
        <source>Chassis Information</source>
        <translation>Informações do Chassi</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="101"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="219"/>
        <source>Physical Memory Array</source>
        <translation>Conjunto de Memória Física</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="163"/>
        <source>Release Date</source>
        <translation>Data de Lançamento</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="164"/>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="165"/>
        <source>Runtime Size</source>
        <translation>Tamanho do Tempo de Execução</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="166"/>
        <source>ROM Size</source>
        <translation>Tamanho da ROM</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="167"/>
        <source>Characteristics</source>
        <translation>Características</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="168"/>
        <source>BIOS Revision</source>
        <translation>Revisão da BIOS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="169"/>
        <source>Firmware Revision</source>
        <translation>Revisão do Firmware</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="171"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="180"/>
        <source>Product Name</source>
        <translation>Nome do Produto</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="172"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="182"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="191"/>
        <source>Serial Number</source>
        <translation>Número de Série</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="173"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="192"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="70"/>
        <source>Asset Tag</source>
        <translation>Etiqueta de Recurso</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="174"/>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="186"/>
        <source>Features</source>
        <translation>Recursos</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="175"/>
        <source>Location In Chassis</source>
        <translation>Localização no Chassi</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="176"/>
        <source>Chassis Handle</source>
        <translation>Identificador do Chassi</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="177"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="189"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="178"/>
        <source>Contained Object Handles</source>
        <translation>Identificador de Objetos Contidos</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="183"/>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="208"/>
        <source>UUID</source>
        <translation>UUID</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="184"/>
        <source>Wake-up Type</source>
        <translation>Tipo de Despertar</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="185"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="201"/>
        <source>SKU Number</source>
        <translation>Número SKU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="186"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="213"/>
        <source>Family</source>
        <translation>Família</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="190"/>
        <source>Lock</source>
        <translation>Bloquear</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="193"/>
        <source>Boot-up State</source>
        <translation>Estado de Inicialização</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="194"/>
        <source>Power Supply State</source>
        <translation>Estado da Fonte de Alimentação</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="195"/>
        <source>Thermal State</source>
        <translation>Estado Térmico</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="196"/>
        <source>Security Status</source>
        <translation>Status de Segurança</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="197"/>
        <source>OEM Information</source>
        <translation>Informações OEM</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="198"/>
        <source>Height</source>
        <translation>Altura</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="199"/>
        <source>Number Of Power Cords</source>
        <translation>Número de Cabos de Alimentação</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="200"/>
        <source>Contained Elements</source>
        <translation>Elementos Contidos</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="203"/>
        <source>Location</source>
        <translation>Localização</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="205"/>
        <source>Error Correction Type</source>
        <translation>Tipo de Erro da Correção</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="206"/>
        <source>Maximum Capacity</source>
        <translation>Capacidade Máxima</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="207"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="65"/>
        <source>Error Information Handle</source>
        <translation>Identificador das Informações de Erro</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="208"/>
        <source>Number Of Devices</source>
        <translation>Número de Dispositivos</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="210"/>
        <source>BIOS ROMSIZE</source>
        <translation>TAMANHO DA ROM DA BIOS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="211"/>
        <source>Release date</source>
        <translation>Data de lançamento</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="212"/>
        <source>Board name</source>
        <translation>Nome da placa</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="221"/>
        <source>SMBIOS Version</source>
        <translation>Versão da SMBIOS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="223"/>
        <source>Language Description Format</source>
        <translation>Formato de Descrição do Idioma</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="224"/>
        <source>Installable Languages</source>
        <translation>Idiomas Instaláveis</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="225"/>
        <source>Currently Installed Language</source>
        <translation>Idioma Atualmente Instalado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="183"/>
        <source>BD Address</source>
        <translation>Endereço BD</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="184"/>
        <source>ACL MTU</source>
        <translation>ACL MTU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="185"/>
        <source>SCO MTU</source>
        <translation>SCO MTU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="187"/>
        <source>Packet type</source>
        <translation>Tipo de pacote</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="188"/>
        <source>Link policy</source>
        <translation>Política de link</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="189"/>
        <source>Link mode</source>
        <translation>Modo de link</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="190"/>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="204"/>
        <source>Class</source>
        <translation>Classe</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="191"/>
        <source>Service Classes</source>
        <translation>Classes de Serviço</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="192"/>
        <source>Device Class</source>
        <translation>Classe de Dispositivo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="193"/>
        <source>HCI Version</source>
        <translation>Versão do HCI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="195"/>
        <source>LMP Version</source>
        <translation>Versão do LMP</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="196"/>
        <source>Subversion</source>
        <translation>Subversão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="198"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="35"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="199"/>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="134"/>
        <source>Serial ID</source>
        <translation>Identificação de Série</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="201"/>
        <source>product</source>
        <translation>produto</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="202"/>
        <source>description</source>
        <translation>descrição</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="205"/>
        <source>Powered</source>
        <translation>Ligado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="206"/>
        <source>Discoverable</source>
        <translation>Detectável</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="207"/>
        <source>Pairable</source>
        <translation>Pareável</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="209"/>
        <source>Modalias</source>
        <translation>Modalias</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="210"/>
        <source>Discovering</source>
        <translation>Procurando</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="136"/>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="202"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="372"/>
        <source>Device File</source>
        <translation>Arquivo do Dispositivo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="137"/>
        <source>Device Files</source>
        <translation>Arquivos do Dispositivo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="138"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="381"/>
        <source>Device Number</source>
        <translation>Número do Dispositivo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="139"/>
        <source>Module Alias</source>
        <translation>Pseudônimo do Módulo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="141"/>
        <source>Application</source>
        <translation>Aplicativo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="144"/>
        <source>status</source>
        <translation>status</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="145"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="375"/>
        <source>logical name</source>
        <translation>nome lógico</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="147"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="373"/>
        <source>ansiversion</source>
        <translation>ansiversion</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="34"/>
        <source>CPU implementer</source>
        <translation>Implementador da CPU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="35"/>
        <source>CPU architecture</source>
        <translation>Arquitetura da CPU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="36"/>
        <source>CPU variant</source>
        <translation>Variante da CPU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="37"/>
        <source>CPU part</source>
        <translation>Peça da CPU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="38"/>
        <source>CPU revision</source>
        <translation>Revisão da CPU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="355"/>
        <source>One</source>
        <translation>Um</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="356"/>
        <source>Two</source>
        <translation>Dois</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="357"/>
        <source>Four</source>
        <translation>Quatro</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="358"/>
        <source>Six</source>
        <translation>Seis</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="359"/>
        <source>Eight</source>
        <translation>Oito</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="361"/>
        <source>Ten</source>
        <translation>Dez</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="362"/>
        <source>Twelve</source>
        <translation>Doze</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="363"/>
        <source>Fourteen</source>
        <translation>Quatorze</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="364"/>
        <source>Sixteen</source>
        <translation>Dezesseis</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="365"/>
        <source>Eighteen</source>
        <translation>Dezoito</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="367"/>
        <source>Twenty</source>
        <translation>Vinte</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="368"/>
        <source>Twenty-two</source>
        <translation>Vinte e Dois</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="369"/>
        <source>Twenty-four</source>
        <translation>Vinte e Quatro</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="370"/>
        <source>Twenty-six</source>
        <translation>Vinte e Seis</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="371"/>
        <source>Twenty-eight</source>
        <translation>Vinte e Oito</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="373"/>
        <source>Thirty</source>
        <translation>Trinta</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="374"/>
        <source>Thirty-two</source>
        <translation>Trinta e Dois</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="375"/>
        <source>Thirty-four</source>
        <translation>Trinta e Quatro</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="376"/>
        <source>Thirty-six</source>
        <translation>Trinta e Seis</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="377"/>
        <source>Thirty-eight</source>
        <translation>Trinta e Oito</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="379"/>
        <source>Forty</source>
        <translation>Quarenta</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="380"/>
        <source>Forty-two</source>
        <translation>Quarenta e Dois</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="381"/>
        <source>Forty-four</source>
        <translation>Quarenta e Quatro</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="382"/>
        <source>Forty-six</source>
        <translation>Quarenta e Seis</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="383"/>
        <source>Forty-eight</source>
        <translation>Quarenta e Oito</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="385"/>
        <source>Fifty</source>
        <translation>Cinquenta</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="386"/>
        <source>Fifty-two</source>
        <translation>Cinquenta e Dois</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="387"/>
        <source>Fifty-four</source>
        <translation>Cinquenta e Quatro</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="388"/>
        <source>Fifty-six</source>
        <translation>Cinquenta e Seis</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="389"/>
        <source>Fifty-eight</source>
        <translation>Cinquenta e Oito</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="391"/>
        <source>Sixty</source>
        <translation>Sessenta</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="392"/>
        <source>Sixty-two</source>
        <translation>Sessenta e Dois</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="393"/>
        <source>Sixty-four</source>
        <translation>Sessenta e Quatro</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="394"/>
        <source>Sixty-six</source>
        <translation>Sessenta e seis</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="395"/>
        <source>Sixty-eight</source>
        <translation>Sessenta e oito</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="397"/>
        <source>Seventy</source>
        <translation>Setenta</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="398"/>
        <source>Seventy-two</source>
        <translation>Setenta e dois</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="399"/>
        <source>Seventy-four</source>
        <translation>Setenta e quatro</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="400"/>
        <source>Seventy-six</source>
        <translation>Setenta e seis</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="401"/>
        <source>Seventy-eight</source>
        <translation>Setenta e oito</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="403"/>
        <source>Eighty</source>
        <translation>Oitenta</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="404"/>
        <source>Eighty-two</source>
        <translation>Oitenta e dois</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="405"/>
        <source>Eighty-four</source>
        <translation>Oitenta e quatro</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="406"/>
        <source>Eighty-six</source>
        <translation>Oitenta e seis</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="407"/>
        <source>Eighty-eight</source>
        <translation>Oitenta e oito</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="409"/>
        <source>Ninety</source>
        <translation>Noventa</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="410"/>
        <source>Ninety-two</source>
        <translation>Noventa e dois</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="411"/>
        <source>Ninety-four</source>
        <translation>Noventa e quatro</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="412"/>
        <source>Ninety-six</source>
        <translation>Noventa e seis</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="413"/>
        <source>Ninety-eight</source>
        <translation>Noventa e oito</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="415"/>
        <source>One hundred</source>
        <translation>Cem</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="416"/>
        <source>One hundred and Two</source>
        <translation>Cento e dois</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="417"/>
        <source>One hundred and four</source>
        <translation>Cento e quatro</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="418"/>
        <source>One hundred and Six</source>
        <translation>Cento e seis</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="419"/>
        <source>One hundred and Eight</source>
        <translation>Cento e oito</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="421"/>
        <source>One hundred and Ten</source>
        <translation>Cento e dez</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="422"/>
        <source>One hundred and Twelve</source>
        <translation>Cento e doze</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="423"/>
        <source>One hundred and Fourteen</source>
        <translation>Cento e quatorze</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="424"/>
        <source>One hundred and Sixteen</source>
        <translation>Cento e dezesseis</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="425"/>
        <source>One hundred and Eighteen</source>
        <translation>Cento e dezoito</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="427"/>
        <source>One hundred and Twenty</source>
        <translation>Cento e vinte</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="428"/>
        <source>One hundred and Twenty-two</source>
        <translation>Cento e vinte e dois</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="429"/>
        <source>One hundred and Twenty-four</source>
        <translation>Cento e vinte e quatro</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="430"/>
        <source>One hundred and Twenty-six</source>
        <translation>Cento e vinte e seis</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="431"/>
        <source>One hundred and Twenty-eight</source>
        <translation>Cento e Vinte e Oito</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="43"/>
        <source>GDDR capacity</source>
        <translation>Capacidade da GDDR</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="44"/>
        <source>GPU vendor</source>
        <translation>Fabricante da GPU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="45"/>
        <source>GPU type</source>
        <translation>Tipo de GPU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="46"/>
        <source>EGL version</source>
        <translation>Versão do EGL</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="47"/>
        <source>EGL client APIs</source>
        <translation>APIs do cliente EGL</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="48"/>
        <source>GL version</source>
        <translation>Versão do GL</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="49"/>
        <source>GLSL version</source>
        <translation>Versão do GLSL</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInfo.cpp" line="69"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="183"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="204"/>
        <source>Unknown</source>
        <translation>Desconhecido</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="201"/>
        <source>MSC</source>
        <translation>MSC</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="203"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="371"/>
        <source>Hardware Class</source>
        <translation>Classe do Hardware</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="698"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="725"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="752"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="787"/>
        <source>CPU</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="698"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="725"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="752"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="787"/>
        <source>No CPU found</source>
        <translation>Nenhuma CPU encontrada</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="699"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="726"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="753"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="788"/>
        <source>Motherboard</source>
        <translation>Placa-mãe</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="699"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="726"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="753"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="788"/>
        <source>No motherboard found</source>
        <translation>Nenhuma placa-mãe encontrada</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="700"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="727"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="754"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="789"/>
        <source>Memory</source>
        <translation>Memória</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="700"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="727"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="754"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="789"/>
        <source>No memory found</source>
        <translation>Nenhuma memória encontrada</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="701"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="728"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="755"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="790"/>
        <source>Storage</source>
        <translation>Armazenamento</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="701"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="728"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="755"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="790"/>
        <source>No disk found</source>
        <translation>Nenhum disco encontrado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="702"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="729"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="756"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="791"/>
        <source>Display Adapter</source>
        <translation>Adaptador de Vídeo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="702"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="729"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="756"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="791"/>
        <source>No GPU found</source>
        <translation>Nenhuma GPU encontrada</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="703"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="730"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="757"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="792"/>
        <source>Monitor</source>
        <translation>Monitor</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="703"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="730"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="757"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="792"/>
        <source>No monitor found</source>
        <translation>Nenhum monitor encontrado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="704"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="731"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="758"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="793"/>
        <source>Network Adapter</source>
        <translation>Adaptador de Rede</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="704"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="731"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="758"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="793"/>
        <source>No network adapter found</source>
        <translation>Nenhuma placa de rede encontrada</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="705"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="732"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="759"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="794"/>
        <source>Sound Adapter</source>
        <translation>Adaptador de Som</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="705"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="732"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="759"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="794"/>
        <source>No audio device found</source>
        <translation>Nenhum dispositivo de áudio encontrado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="706"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="733"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="760"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="795"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="706"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="733"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="760"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="795"/>
        <source>No Bluetooth device found</source>
        <translation>Nenhum dispositivo Bluetooth encontrado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="707"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="734"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="761"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="796"/>
        <source>Other PCI Devices</source>
        <translation>Outros Dispositivos PCI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="707"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="734"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="761"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="796"/>
        <source>No other PCI devices found</source>
        <translation>Nenhum dispositivo PCI encontrado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="708"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="735"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="762"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="797"/>
        <source>Power</source>
        <translation>Energia</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="708"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="735"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="762"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="797"/>
        <source>No battery found</source>
        <translation>Nenhuma bateria encontrada</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="709"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="736"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="763"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="798"/>
        <source>Keyboard</source>
        <translation>Teclado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="709"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="736"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="763"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="798"/>
        <source>No keyboard found</source>
        <translation>Nenhum teclado encontrado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="710"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="737"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="764"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="799"/>
        <source>Mouse</source>
        <translation>Mouse</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="710"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="737"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="764"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="799"/>
        <source>No mouse found</source>
        <translation>Nenhum mouse encontrado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="711"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="738"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="765"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="800"/>
        <source>Printer</source>
        <translation>Impressora</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="711"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="738"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="765"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="800"/>
        <source>No printer found</source>
        <translation>Nenhuma impressora encontrada</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="712"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="739"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="766"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="801"/>
        <source>Camera</source>
        <translation>Câmera</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="712"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="739"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="766"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="801"/>
        <source>No camera found</source>
        <translation>Nenhuma câmera encontrada</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="713"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="740"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="767"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="802"/>
        <source>CD-ROM</source>
        <translation>CD-ROM</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="713"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="740"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="767"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="802"/>
        <source>No CD-ROM found</source>
        <translation>Nenhum CD-ROM encontrado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="714"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="741"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="768"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="803"/>
        <source>Other Devices</source>
        <translation>Outros Dispositivos</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="714"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="741"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="768"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="803"/>
        <source>No other devices found</source>
        <translation>Nenhum dispositivo encontrado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="64"/>
        <source>Array Handle</source>
        <translation>Controlador de Matriz</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="66"/>
        <source>Form Factor</source>
        <translation>Fator de Forma</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="67"/>
        <source>Set</source>
        <translation>Definir</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="68"/>
        <source>Bank Locator</source>
        <translation>Localizador de Banco</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="69"/>
        <source>Type Detail</source>
        <translation>Detalhes do Tipo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="71"/>
        <source>Part Number</source>
        <translation>Número da Peça</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="72"/>
        <source>Rank</source>
        <translation>Rank</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="73"/>
        <source>Memory Technology</source>
        <translation>Tecnologia da Memória</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="74"/>
        <source>Memory Operating Mode Capability</source>
        <translation>Capacidade do Modo de Operação da Memória</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="75"/>
        <source>Firmware Version</source>
        <translation>Versão do Firmware</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="76"/>
        <source>Module Manufacturer ID</source>
        <translation>ID do Fabricante do Módulo</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="77"/>
        <source>Module Product ID</source>
        <translation>ID do Módulo de Produto</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="78"/>
        <source>Memory Subsystem Controller Manufacturer ID</source>
        <translation>ID do Fabricante do Controlador do Subsistema de Memória</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="79"/>
        <source>Memory Subsystem Controller Product ID</source>
        <translation>ID do Produto do Controlador do Subsistema de Memória</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="80"/>
        <source>Non-Volatile Size</source>
        <translation>Tamanho Não Volátil</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="81"/>
        <source>Volatile Size</source>
        <translation>Tamanho Volátil</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="82"/>
        <source>Cache Size</source>
        <translation>Tamanho do Cache</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="83"/>
        <source>Logical Size</source>
        <translation>Tamanho Lógico</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="43"/>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="56"/>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="382"/>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="409"/>
        <source>inch</source>
        <translation>pol</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="260"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="227"/>
        <source>ioport</source>
        <translation>ioport</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="229"/>
        <source>network</source>
        <translation>rede</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="38"/>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="64"/>
        <source>battery</source>
        <translation>bateria</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="181"/>
        <source>native-path</source>
        <translation>native-path</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="182"/>
        <source>power supply</source>
        <translation>fonte de energia</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="183"/>
        <source>updated</source>
        <translation>atualizado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="184"/>
        <source>has history</source>
        <translation>possui histórico</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="185"/>
        <source>has statistics</source>
        <translation>possui estatísticas</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="186"/>
        <source>rechargeable</source>
        <translation>recarregável</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="187"/>
        <source>state</source>
        <translation>estado</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="188"/>
        <source>warning-level</source>
        <translation>warning-level</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="189"/>
        <source>energy</source>
        <translation>energia</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="190"/>
        <source>energy-empty</source>
        <translation>energy-empty</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="191"/>
        <source>energy-full</source>
        <translation>energy-full</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="192"/>
        <source>energy-full-design</source>
        <translation>energy-full-design</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="193"/>
        <source>energy-rate</source>
        <translation>energy-rate</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="194"/>
        <source>voltage</source>
        <translation>tensão</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="195"/>
        <source>percentage</source>
        <translation>porcentagem</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="196"/>
        <source>temperature</source>
        <translation>temperatura</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="197"/>
        <source>technology</source>
        <translation>tecnologia</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="198"/>
        <source>icon-name</source>
        <translation>icon-name</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="199"/>
        <source>online</source>
        <translation>on-line</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="200"/>
        <source>daemon-version</source>
        <translation>daemon-version</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="201"/>
        <source>on-battery</source>
        <translation>on-battery</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="202"/>
        <source>lid-is-closed</source>
        <translation>lid-is-closed</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="203"/>
        <source>lid-is-present</source>
        <translation>lid-is-present</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="204"/>
        <source>critical-action</source>
        <translation>critical-action</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="116"/>
        <source>copies</source>
        <translation>cópias</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="119"/>
        <source>job-cancel-after</source>
        <translation>job-cancel-after</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="120"/>
        <source>job-hold-until</source>
        <translation>job-hold-until</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="121"/>
        <source>job-priority</source>
        <translation>job-priority</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="123"/>
        <source>marker-change-time</source>
        <translation>marker-change-time</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="126"/>
        <source>number-up</source>
        <translation>number-up</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="127"/>
        <source>orientation-requested</source>
        <translation>orientation-requested</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="128"/>
        <source>print-color-mode</source>
        <translation>print-color-mode</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="132"/>
        <source>printer-is-accepting-jobs</source>
        <translation>printer-is-accepting-jobs</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="133"/>
        <source>printer-is-shared</source>
        <translation>printer-is-shared</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="134"/>
        <source>printer-is-temporary</source>
        <translation>printer-is-temporary</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="136"/>
        <source>printer-make-and-model</source>
        <translation>printer-make-and-model</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="138"/>
        <source>printer-state-change-time</source>
        <translation>printer-state-change-time</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="139"/>
        <source>printer-state-reasons</source>
        <translation>printer-state-reasons</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="140"/>
        <source>printer-type</source>
        <translation>printer-type</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="141"/>
        <source>printer-uri-supported</source>
        <translation>printer-uri-supported</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="142"/>
        <source>sides</source>
        <translation>sides</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="179"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="200"/>
        <source>SSD</source>
        <translation>SSD</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="181"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="202"/>
        <source>HDD</source>
        <translation>HDD</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="374"/>
        <source>bus info</source>
        <translation>informações do barramento</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="376"/>
        <source>logicalsectorsize</source>
        <translation>logicalsectorsize</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="378"/>
        <source>sectorsize</source>
        <translation>sectorsize</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="379"/>
        <source>guid</source>
        <translation>guid</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="382"/>
        <source>Geometry (Logical)</source>
        <translation>Geometria (lógica)</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="46"/>
        <location filename="../src/main.cpp" line="50"/>
        <source>Device Manager</source>
        <translation>Gerenciador de Dispositivos</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="52"/>
        <source>Device Manager is a handy tool for viewing hardware information and managing the devices.</source>
        <translation>O Gerenciador de Dispositivos é uma ferramenta útil para visualizar as informações de hardware e gerenciar os dispositivos.</translation>
    </message>
</context>
<context>
    <name>TableWidget</name>
    <message>
        <location filename="../src/Widget/TableWidget.cpp" line="23"/>
        <location filename="../src/Widget/TableWidget.cpp" line="135"/>
        <source>Disable</source>
        <translation>Desativar</translation>
    </message>
    <message>
        <location filename="../src/Widget/TableWidget.cpp" line="24"/>
        <source>Refresh</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <location filename="../src/Widget/TableWidget.cpp" line="25"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../src/Widget/TableWidget.cpp" line="137"/>
        <location filename="../src/Widget/TableWidget.cpp" line="167"/>
        <source>Enable</source>
        <translation>Ativar</translation>
    </message>
</context>
<context>
    <name>TextBrowser</name>
    <message>
        <location filename="../src/Widget/TextBrowser.cpp" line="19"/>
        <source>Refresh</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <location filename="../src/Widget/TextBrowser.cpp" line="20"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../src/Widget/TextBrowser.cpp" line="21"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../src/Widget/TextBrowser.cpp" line="254"/>
        <source>Disable</source>
        <translation>Desativar</translation>
    </message>
</context>
<context>
    <name>ThreadPool</name>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="218"/>
        <source>Loading Audio Device Info...</source>
        <translation>Carregando Informações do Dispositivo de Áudio...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="221"/>
        <source>Loading BIOS Info...</source>
        <translation>Carregando Informações da BIOS...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="231"/>
        <source>Loading CD-ROM Info...</source>
        <translation>Carregando Informações do CD-ROM...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="235"/>
        <source>Loading Bluetooth Device Info...</source>
        <translation>Carregando Informações do Dispositivo Bluetooth...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="236"/>
        <source>Loading Image Devices Info...</source>
        <translation>Carregando Informações dos Dispositivos de Imagem...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="238"/>
        <source>Loading Keyboard Info...</source>
        <translation>Carregando Informações do Teclado...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="244"/>
        <source>Loading Operating System Info...</source>
        <translation>Carregando Informações do Sistema Operacional...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="246"/>
        <source>Loading CPU Info...</source>
        <translation>Carregando Informações da CPU...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="247"/>
        <source>Loading Other Devices Info...</source>
        <translation>Carregando Informações de Outros Dispositivos...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="248"/>
        <source>Loading Power Info...</source>
        <translation>Carregando Informações de Energia...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="249"/>
        <source>Loading Printer Info...</source>
        <translation>Carregando Informações da Impressora...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="256"/>
        <source>Loading Monitor Info...</source>
        <translation>Carregando Informações do Monitor...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="257"/>
        <source>Loading Mouse Info...</source>
        <translation>Carregando Informações do Mouse...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="258"/>
        <source>Loading Network Adapter Info...</source>
        <translation>Carregando Informações da Placa de Rede...</translation>
    </message>
</context>
<context>
    <name>WaitingWidget</name>
    <message>
        <location filename="../src/Page/WaitingWidget.cpp" line="15"/>
        <source>Loading...</source>
        <translation>Carregando...</translation>
    </message>
</context>
</TS>