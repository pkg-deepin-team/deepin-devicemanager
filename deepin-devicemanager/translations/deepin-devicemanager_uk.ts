<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.1">
<context>
    <name>CmdButtonWidget</name>
    <message>
        <location filename="../src/Widget/CmdButtonWidget.cpp" line="14"/>
        <source>More</source>
        <translation>Більше</translation>
    </message>
</context>
<context>
    <name>DetailButton</name>
    <message>
        <location filename="../src/Page/PageDetail.cpp" line="33"/>
        <location filename="../src/Page/PageDetail.cpp" line="36"/>
        <source>More</source>
        <translation>Більше</translation>
    </message>
    <message>
        <location filename="../src/Page/PageDetail.cpp" line="34"/>
        <source>Collapse</source>
        <translation>Згорнути</translation>
    </message>
</context>
<context>
    <name>DetailTreeView</name>
    <message>
        <location filename="../src/Widget/DetailTreeView.cpp" line="83"/>
        <location filename="../src/Widget/DetailTreeView.cpp" line="249"/>
        <source>More</source>
        <translation>Більше</translation>
    </message>
    <message>
        <location filename="../src/Widget/DetailTreeView.cpp" line="255"/>
        <source>Collapse</source>
        <translation>Згорнути</translation>
    </message>
</context>
<context>
    <name>DetailViewDelegate</name>
    <message>
        <location filename="../src/Widget/DetailViewDelegate.cpp" line="226"/>
        <location filename="../src/Widget/DetailViewDelegate.cpp" line="230"/>
        <source>Disable</source>
        <translation>Вимкнути</translation>
    </message>
</context>
<context>
    <name>DeviceAudio</name>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="221"/>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="299"/>
        <source>Disable</source>
        <translation>Вимкнути</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="237"/>
        <source>Device Name</source>
        <translation>Назва пристрою</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="266"/>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="290"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="267"/>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="291"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="268"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="269"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="270"/>
        <source>Bus Info</source>
        <translation>Дані щодо каналу</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="276"/>
        <source>Chip</source>
        <translation>Мікросхема</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="277"/>
        <source>Capabilities</source>
        <translation>Можливості</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="278"/>
        <source>Clock</source>
        <translation>Годинник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="279"/>
        <source>Width</source>
        <translation>Ширина</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="280"/>
        <source>Memory</source>
        <translation>Пам&apos;ять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="281"/>
        <source>IRQ</source>
        <translation>IRQ</translation>
    </message>
</context>
<context>
    <name>DeviceBaseInfo</name>
    <message>
        <location filename="../src/DeviceManager/DeviceInfo.cpp" line="441"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInfo.cpp" line="442"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInfo.cpp" line="443"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
</context>
<context>
    <name>DeviceBios</name>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="231"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="232"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="233"/>
        <source>Chipset</source>
        <translation>Набір мікросхем</translation>
    </message>
</context>
<context>
    <name>DeviceBluetooth</name>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="216"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="217"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="218"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="219"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="244"/>
        <source>Speed</source>
        <translation>Швидкість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="245"/>
        <source>Maximum Power</source>
        <translation>Максимальна потужність</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="246"/>
        <source>Driver Version</source>
        <translation>Версія драйвера</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="247"/>
        <source>Driver</source>
        <translation>Драйвер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="248"/>
        <source>Capabilities</source>
        <translation>Можливості</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="249"/>
        <source>Bus Info</source>
        <translation>Дані щодо каналу</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="250"/>
        <source>Logical Name</source>
        <translation>Логічна назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="251"/>
        <source>MAC Address</source>
        <translation>MAC-адреса</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="262"/>
        <source>Disable</source>
        <translation>Вимкнути</translation>
    </message>
</context>
<context>
    <name>DeviceCdrom</name>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="153"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="154"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="155"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="156"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="157"/>
        <source>Bus Info</source>
        <translation>Дані щодо каналу</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="158"/>
        <source>Capabilities</source>
        <translation>Можливості</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="159"/>
        <source>Driver</source>
        <translation>Драйвер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="160"/>
        <source>Maximum Power</source>
        <translation>Максимальна потужність</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="161"/>
        <source>Speed</source>
        <translation>Швидкість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="175"/>
        <source>Disable</source>
        <translation>Вимкнути</translation>
    </message>
</context>
<context>
    <name>DeviceComputer</name>
    <message>
        <location filename="../src/DeviceManager/DeviceComputer.cpp" line="165"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
</context>
<context>
    <name>DeviceCpu</name>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="44"/>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="337"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="45"/>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="338"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="46"/>
        <source>CPU ID</source>
        <translation>Ід. проц.</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="47"/>
        <source>Core ID</source>
        <translation>ID ядра</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="48"/>
        <source>Threads</source>
        <translation>Потоки</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="49"/>
        <source>Current Speed</source>
        <translation>Поточна швидкість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="50"/>
        <source>BogoMIPS</source>
        <translation>BogoMIPS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="51"/>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="340"/>
        <source>Architecture</source>
        <translation>Архітектура</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="52"/>
        <source>CPU Family</source>
        <translation>Сімейство проц.</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="53"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="185"/>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="197"/>
        <source>Processor</source>
        <translation>Процесор</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="195"/>
        <source>Core(s)</source>
        <translation>Ядра</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="321"/>
        <source>Virtualization</source>
        <translation>Віртуалізація</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="322"/>
        <source>Flags</source>
        <translation>Прапорці</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="323"/>
        <source>Extensions</source>
        <translation>Розширення</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="324"/>
        <source>L3 Cache</source>
        <translation>Кеш L3</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="325"/>
        <source>L2 Cache</source>
        <translation>Кеш L2</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="326"/>
        <source>L1i Cache</source>
        <translation>Кеш L1i</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="327"/>
        <source>L1d Cache</source>
        <translation>Кеш L1d</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="328"/>
        <source>Stepping</source>
        <translation>Модифікація</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="339"/>
        <source>Speed</source>
        <translation>Швидкість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="339"/>
        <source>Max Speed</source>
        <translation>Макс. швидкість</translation>
    </message>
</context>
<context>
    <name>DeviceGpu</name>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="55"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="56"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="57"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="58"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="59"/>
        <source>Graphics Memory</source>
        <translation>Графічна пам&apos;ять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="313"/>
        <source>Physical ID</source>
        <translation>Фізичний ID</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="314"/>
        <source>Memory</source>
        <translation>Пам&apos;ять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="315"/>
        <source>IO Port</source>
        <translation>Порт ВВ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="316"/>
        <source>Bus Info</source>
        <translation>Дані щодо каналу</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="317"/>
        <source>Maximum Resolution</source>
        <translation>Максимальна роздільність</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="318"/>
        <source>Minimum Resolution</source>
        <translation>Мінімальна роздільність</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="319"/>
        <source>Current Resolution</source>
        <translation>Поточна роздільність</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="320"/>
        <source>Driver</source>
        <translation>Драйвер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="321"/>
        <source>Description</source>
        <translation>Опис</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="322"/>
        <source>Clock</source>
        <translation>Годинник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="323"/>
        <source>DP</source>
        <translation>DP</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="324"/>
        <source>eDP</source>
        <translation>eDP</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="325"/>
        <source>HDMI</source>
        <translation>HDMI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="326"/>
        <source>VGA</source>
        <translation>VGA</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="327"/>
        <source>DVI</source>
        <translation>DVI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="328"/>
        <source>Display Output</source>
        <translation>Виведення на дисплей</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="329"/>
        <source>Capabilities</source>
        <translation>Можливості</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="330"/>
        <source>IRQ</source>
        <translation>IRQ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="331"/>
        <source>Width</source>
        <translation>Ширина</translation>
    </message>
</context>
<context>
    <name>DeviceImage</name>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="148"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="149"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="150"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="151"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="152"/>
        <source>Bus Info</source>
        <translation>Дані щодо каналу</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="158"/>
        <source>Speed</source>
        <translation>Швидкість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="159"/>
        <source>Maximum Power</source>
        <translation>Максимальна потужність</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="160"/>
        <source>Driver</source>
        <translation>Драйвер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="161"/>
        <source>Capabilities</source>
        <translation>Можливості</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="172"/>
        <source>Disable</source>
        <translation>Вимкнути</translation>
    </message>
</context>
<context>
    <name>DeviceInput</name>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="211"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="212"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="213"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="214"/>
        <source>Interface</source>
        <translation>Інтерфейс</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="215"/>
        <source>Bus Info</source>
        <translation>Дані щодо каналу</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="221"/>
        <source>Speed</source>
        <translation>Швидкість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="222"/>
        <source>Maximum Power</source>
        <translation>Максимальна потужність</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="223"/>
        <source>Driver</source>
        <translation>Драйвер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="224"/>
        <source>Capabilities</source>
        <translation>Можливості</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="225"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="235"/>
        <source>Disable</source>
        <translation>Вимкнути</translation>
    </message>
</context>
<context>
    <name>DeviceManager</name>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="136"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="190"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="820"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="839"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="859"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="867"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="882"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="892"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="907"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="918"/>
        <source>Overview</source>
        <translation>Огляд</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="138"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="169"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="991"/>
        <source>CPU</source>
        <translation>Процесор</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="141"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="170"/>
        <source>Motherboard</source>
        <translation>Материнська плата</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="142"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="171"/>
        <source>Memory</source>
        <translation>Пам&apos;ять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="143"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="172"/>
        <source>Display Adapter</source>
        <translation>Адаптер дисплея</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="144"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="173"/>
        <source>Sound Adapter</source>
        <translation>Звуковий адаптер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="145"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="174"/>
        <source>Storage</source>
        <translation>Сховище даних</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="146"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="175"/>
        <source>Other PCI Devices</source>
        <translation>Інші пристрої PCI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="147"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="176"/>
        <source>Battery</source>
        <translation>Акумулятор</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="150"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="177"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="151"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="178"/>
        <source>Network Adapter</source>
        <translation>Адаптер мережі</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="154"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="179"/>
        <source>Mouse</source>
        <translation>Миша</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="155"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="180"/>
        <source>Keyboard</source>
        <translation>Клавіатура</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="158"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="181"/>
        <source>Monitor</source>
        <translation>Монітор</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="159"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="182"/>
        <source>CD-ROM</source>
        <translation>CD-ROM</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="160"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="183"/>
        <source>Printer</source>
        <translation>Принтер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="161"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="184"/>
        <source>Camera</source>
        <translation>Камера</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="162"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="185"/>
        <source>Other Devices</source>
        <comment>Other Input Devices</comment>
        <translation>Інші пристрої</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="825"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="885"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="912"/>
        <source>Device</source>
        <translation>Пристрій</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="832"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="888"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="914"/>
        <source>OS</source>
        <translation>ОС</translation>
    </message>
</context>
<context>
    <name>DeviceMemory</name>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="89"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="115"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="90"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="116"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="91"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="119"/>
        <source>Size</source>
        <translation>Розмір</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="92"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="117"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="93"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="118"/>
        <source>Speed</source>
        <translation>Швидкість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="94"/>
        <source>Total Width</source>
        <translation>Загальна ширина</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="95"/>
        <source>Locator</source>
        <translation>Локатор</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="96"/>
        <source>Serial Number</source>
        <translation>Серійний номер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="103"/>
        <source>Configured Voltage</source>
        <translation>Поточне напруження</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="104"/>
        <source>Maximum Voltage</source>
        <translation>Максимальна напруга</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="105"/>
        <source>Minimum Voltage</source>
        <translation>Мінімальна напруга</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="106"/>
        <source>Configured Speed</source>
        <translation>Налаштована швидкість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="107"/>
        <source>Data Width</source>
        <translation>Ширина даних</translation>
    </message>
</context>
<context>
    <name>DeviceMonitor</name>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="266"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="267"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="268"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="269"/>
        <source>Display Input</source>
        <translation>Вхід дисплея</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="270"/>
        <source>Interface Type</source>
        <translation>Тип інтерфейсу</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="276"/>
        <source>Support Resolution</source>
        <translation>Підтримувана роздільність</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="277"/>
        <source>Current Resolution</source>
        <translation>Поточна роздільність</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="278"/>
        <source>Primary Monitor</source>
        <translation>Основний монітор</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="279"/>
        <source>Display Ratio</source>
        <translation>Співвідношення розмірів</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="280"/>
        <source>Size</source>
        <translation>Розмір</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="281"/>
        <source>Serial Number</source>
        <translation>Серійний номер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="282"/>
        <source>Product Date</source>
        <translation>Дата продукту</translation>
    </message>
</context>
<context>
    <name>DeviceNetwork</name>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="235"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="236"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="237"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="238"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="239"/>
        <source>Bus Info</source>
        <translation>Дані щодо каналу</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="240"/>
        <source>Capabilities</source>
        <translation>Можливості</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="241"/>
        <source>Driver</source>
        <translation>Драйвер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="242"/>
        <source>Driver Version</source>
        <translation>Версія драйвера</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="248"/>
        <source>Capacity</source>
        <translation>Місткість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="249"/>
        <source>Speed</source>
        <translation>Швидкість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="250"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="251"/>
        <source>Multicast</source>
        <translation>Групова трансляція</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="252"/>
        <source>Link</source>
        <translation>Зв&apos;язок</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="253"/>
        <source>Latency</source>
        <translation>Латентність</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="254"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="255"/>
        <source>Firmware</source>
        <translation>Мікропрограма</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="256"/>
        <source>Duplex</source>
        <translation>Двобічний</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="257"/>
        <source>Broadcast</source>
        <translation>Трансляція</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="258"/>
        <source>Auto Negotiation</source>
        <translation>Автоузгодження</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="259"/>
        <source>Clock</source>
        <translation>Годинник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="260"/>
        <source>Width</source>
        <translation>Ширина</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="261"/>
        <source>Memory</source>
        <translation>Пам&apos;ять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="262"/>
        <source>IRQ</source>
        <translation>IRQ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="263"/>
        <source>MAC Address</source>
        <translation>MAC-адреса</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="264"/>
        <source>Logical Name</source>
        <translation>Логічна назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="274"/>
        <source>Disable</source>
        <translation>Вимкнути</translation>
    </message>
</context>
<context>
    <name>DeviceOtherPCI</name>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="111"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="112"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="113"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="114"/>
        <source>Bus Info</source>
        <translation>Дані щодо каналу</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="115"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="121"/>
        <source>Input/Output</source>
        <translation>Вхід/Вихід</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="122"/>
        <source>Memory</source>
        <translation>Пам&apos;ять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="123"/>
        <source>IRQ</source>
        <translation>IRQ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="124"/>
        <source>Latency</source>
        <translation>Латентність</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="125"/>
        <source>Clock</source>
        <translation>Годинник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="126"/>
        <source>Width</source>
        <translation>Ширина</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="127"/>
        <source>Driver</source>
        <translation>Драйвер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="128"/>
        <source>Capabilities</source>
        <translation>Можливості</translation>
    </message>
</context>
<context>
    <name>DeviceOthers</name>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="130"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="131"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="132"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="133"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="134"/>
        <source>Bus Info</source>
        <translation>Дані щодо каналу</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="135"/>
        <source>Capabilities</source>
        <translation>Можливості</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="136"/>
        <source>Driver</source>
        <translation>Драйвер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="137"/>
        <source>Maximum Power</source>
        <translation>Максимальна потужність</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="138"/>
        <source>Speed</source>
        <translation>Швидкість</translation>
    </message>
</context>
<context>
    <name>DevicePower</name>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="210"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="211"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="212"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="213"/>
        <source>Serial Number</source>
        <translation>Серійний номер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="214"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="215"/>
        <source>Status</source>
        <translation>Стан</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="216"/>
        <source>Capacity</source>
        <translation>Місткість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="217"/>
        <source>Voltage</source>
        <translation>Напруга</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="218"/>
        <source>Slot</source>
        <translation>Слот</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="219"/>
        <source>Design Capacity</source>
        <translation>Проєктна місткість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="220"/>
        <source>Design Voltage</source>
        <translation>Проєктна напруга</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="221"/>
        <source>SBDS Version</source>
        <translation>Версія SBDS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="222"/>
        <source>SBDS Serial Number</source>
        <translation>Серійний номер SBDS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="223"/>
        <source>SBDS Manufacture Date</source>
        <translation>Дата вироблення SBDS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="224"/>
        <source>SBDS Chemistry</source>
        <translation>Хімія SBDS</translation>
    </message>
</context>
<context>
    <name>DevicePrint</name>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="148"/>
        <source>Name</source>
        <translation>Назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="149"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="150"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="151"/>
        <source>Serial Number</source>
        <translation>Серійний номер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="157"/>
        <source>Shared</source>
        <translation>Спільний</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="158"/>
        <source>URI</source>
        <translation>Адреса</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="159"/>
        <source>Status</source>
        <translation>Стан</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="160"/>
        <source>Interface Type</source>
        <translation>Тип інтерфейсу</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="170"/>
        <source>Disable</source>
        <translation>Вимкнути</translation>
    </message>
</context>
<context>
    <name>DeviceStorage</name>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="390"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="416"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="391"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="417"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="392"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="418"/>
        <source>Media Type</source>
        <translation>Тип носія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="393"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="419"/>
        <source>Size</source>
        <translation>Розмір</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="394"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="395"/>
        <source>Capabilities</source>
        <translation>Можливості</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="401"/>
        <source>Power Cycle Count</source>
        <translation>Кількість циклів вмикання</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="402"/>
        <source>Power On Hours</source>
        <translation>Тривалість роботи у годинах</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="403"/>
        <source>Firmware Version</source>
        <translation>Версія мікропрограми</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="404"/>
        <source>Speed</source>
        <translation>Швидкість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="405"/>
        <source>Description</source>
        <translation>Опис</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="406"/>
        <source>Serial Number</source>
        <translation>Серійний номер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="407"/>
        <source>Interface</source>
        <translation>Інтерфейс</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="408"/>
        <source>Rotation Rate</source>
        <translation>Частота обертання</translation>
    </message>
</context>
<context>
    <name>LogTreeView</name>
    <message>
        <location filename="../src/Widget/logtreeview.cpp" line="82"/>
        <location filename="../src/Widget/logtreeview.cpp" line="101"/>
        <location filename="../src/Widget/logtreeview.cpp" line="103"/>
        <source>Disable</source>
        <translation>Вимкнути</translation>
    </message>
</context>
<context>
    <name>LogViewItemDelegate</name>
    <message>
        <location filename="../src/Widget/logviewitemdelegate.cpp" line="158"/>
        <source>Disable</source>
        <translation>Вимкнути</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="101"/>
        <source>Device Info</source>
        <comment>export file's name</comment>
        <translation>Дані щодо пристрою</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="174"/>
        <source>Display shortcuts</source>
        <translation>Скорочення дисплея</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="175"/>
        <source>Close</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="176"/>
        <source>Help</source>
        <translation>Довідка</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="177"/>
        <source>Copy</source>
        <translation>Копіювати</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="181"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="188"/>
        <source>Export</source>
        <translation>Експортувати</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="189"/>
        <source>Refresh</source>
        <translation>Оновити</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="193"/>
        <source>Device Manager</source>
        <translation>Керування пристроями</translation>
    </message>
</context>
<context>
    <name>PageDetail</name>
    <message>
        <location filename="../src/Page/PageDetail.cpp" line="284"/>
        <source>More</source>
        <translation>Більше</translation>
    </message>
</context>
<context>
    <name>PageListView</name>
    <message>
        <location filename="../src/Page/PageListView.cpp" line="14"/>
        <source>Refresh</source>
        <translation>Оновити</translation>
    </message>
    <message>
        <location filename="../src/Page/PageListView.cpp" line="15"/>
        <source>Export</source>
        <translation>Експортувати</translation>
    </message>
    <message>
        <location filename="../src/Page/PageListView.cpp" line="17"/>
        <source>Overview</source>
        <translation>Огляд</translation>
    </message>
</context>
<context>
    <name>PageMultiInfo</name>
    <message>
        <location filename="../src/Page/PageMultiInfo.cpp" line="95"/>
        <source>Failed to enable the device</source>
        <translation>Не вдалося увімкнути пристрій</translation>
    </message>
    <message>
        <location filename="../src/Page/PageMultiInfo.cpp" line="97"/>
        <source>Failed to disable the device</source>
        <translation>Не вдалося вимкнути пристрій</translation>
    </message>
</context>
<context>
    <name>PageOverview</name>
    <message>
        <location filename="../src/Page/PageOverview.cpp" line="27"/>
        <source>Refresh</source>
        <translation>Оновити</translation>
    </message>
    <message>
        <location filename="../src/Page/PageOverview.cpp" line="28"/>
        <source>Export</source>
        <translation>Експортувати</translation>
    </message>
    <message>
        <location filename="../src/Page/PageOverview.cpp" line="29"/>
        <source>Copy</source>
        <translation>Копіювати</translation>
    </message>
    <message>
        <location filename="../src/Page/PageOverview.cpp" line="61"/>
        <source>Overview</source>
        <translation>Огляд</translation>
    </message>
</context>
<context>
    <name>PageSingleInfo</name>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="21"/>
        <source>Refresh</source>
        <translation>Оновити</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="22"/>
        <source>Export</source>
        <translation>Експортувати</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="23"/>
        <source>Copy</source>
        <translation>Копіювати</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="24"/>
        <location filename="../src/Page/PageSingleInfo.cpp" line="77"/>
        <location filename="../src/Page/PageSingleInfo.cpp" line="120"/>
        <location filename="../src/Page/PageSingleInfo.cpp" line="153"/>
        <source>Enable</source>
        <translation>Увімкнути</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="118"/>
        <location filename="../src/Page/PageSingleInfo.cpp" line="168"/>
        <source>Disable</source>
        <translation>Вимкнути</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="159"/>
        <source>Failed to disable the device</source>
        <translation>Не вдалося вимкнути пристрій</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="174"/>
        <source>Failed to enable the device</source>
        <translation>Не вдалося увімкнути пристрій</translation>
    </message>
</context>
<context>
    <name>PageTableHeader</name>
    <message>
        <location filename="../src/Page/PageTableHeader.cpp" line="61"/>
        <source>Disable</source>
        <translation>Вимкнути</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="238"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="36"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="384"/>
        <source>SubVendor</source>
        <translation>Підрядчик</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="239"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="37"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="383"/>
        <source>SubDevice</source>
        <translation>Підпристрій</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="240"/>
        <source>Driver</source>
        <translation>Драйвер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="241"/>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="135"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="38"/>
        <source>Driver Modules</source>
        <translation>Модулі драйвера</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="242"/>
        <source>Driver Status</source>
        <translation>Стан драйвера</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="243"/>
        <source>Driver Activation Cmd</source>
        <translation>Команда активації драйвера</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="244"/>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="140"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="39"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="380"/>
        <source>Config Status</source>
        <translation>Стан налаштувань</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="247"/>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="203"/>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="142"/>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="205"/>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="228"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="377"/>
        <source>physical id</source>
        <translation>фізичний ід.</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="248"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="40"/>
        <source>latency</source>
        <translation>латентність</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="250"/>
        <source>Phys</source>
        <translation>Фіз.</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="251"/>
        <source>Sysfs</source>
        <translation>Sysfs</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="252"/>
        <source>Handlers</source>
        <translation>Обробники</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="253"/>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="198"/>
        <source>PROP</source>
        <translation>PROP</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="254"/>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="199"/>
        <source>EV</source>
        <translation>EV</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="255"/>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="200"/>
        <source>KEY</source>
        <translation>КЛЮЧ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="257"/>
        <source>Model</source>
        <translation>Модель</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="258"/>
        <source>Vendor</source>
        <translation>Постачальник</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="259"/>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="260"/>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="182"/>
        <source>Bus</source>
        <translation>Канал</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="25"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="215"/>
        <source>BIOS Information</source>
        <translation>Дані BIOS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="47"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="216"/>
        <source>Base Board Information</source>
        <translation>Базові дані щодо плати</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="69"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="217"/>
        <source>System Information</source>
        <translation>Системна інформація</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="85"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="218"/>
        <source>Chassis Information</source>
        <translation>Дані щодо основи</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="101"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="219"/>
        <source>Physical Memory Array</source>
        <translation>Масив фізичної пам&apos;яті</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="163"/>
        <source>Release Date</source>
        <translation>Дата випуску</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="164"/>
        <source>Address</source>
        <translation>Адреса</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="165"/>
        <source>Runtime Size</source>
        <translation>Динамічний розмір</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="166"/>
        <source>ROM Size</source>
        <translation>Розмір ROM</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="167"/>
        <source>Characteristics</source>
        <translation>Характеристики</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="168"/>
        <source>BIOS Revision</source>
        <translation>Модифікація BIOS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="169"/>
        <source>Firmware Revision</source>
        <translation>Модифікація мікропрограми</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="171"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="180"/>
        <source>Product Name</source>
        <translation>Назва продукту</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="172"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="182"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="191"/>
        <source>Serial Number</source>
        <translation>Серійний номер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="173"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="192"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="70"/>
        <source>Asset Tag</source>
        <translation>Мітка активу</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="174"/>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="186"/>
        <source>Features</source>
        <translation>Можливості</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="175"/>
        <source>Location In Chassis</source>
        <translation>Розташування на основі</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="176"/>
        <source>Chassis Handle</source>
        <translation>Обробник основи</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="177"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="189"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="178"/>
        <source>Contained Object Handles</source>
        <translation>Обробники вміщених об&apos;єктів</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="183"/>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="208"/>
        <source>UUID</source>
        <translation>UUID</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="184"/>
        <source>Wake-up Type</source>
        <translation>Тип пробудження</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="185"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="201"/>
        <source>SKU Number</source>
        <translation>Номер SKU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="186"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="213"/>
        <source>Family</source>
        <translation>Сімейство</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="190"/>
        <source>Lock</source>
        <translation>Заблокувати</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="193"/>
        <source>Boot-up State</source>
        <translation>Стан завантаження</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="194"/>
        <source>Power Supply State</source>
        <translation>Стан живлення</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="195"/>
        <source>Thermal State</source>
        <translation>Термічний стан</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="196"/>
        <source>Security Status</source>
        <translation>Стан безпеки</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="197"/>
        <source>OEM Information</source>
        <translation>Дані щодо OEM</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="198"/>
        <source>Height</source>
        <translation>Висота</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="199"/>
        <source>Number Of Power Cords</source>
        <translation>Кількість ліній живлення</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="200"/>
        <source>Contained Elements</source>
        <translation>Включені елементи</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="203"/>
        <source>Location</source>
        <translation>Розташування</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="205"/>
        <source>Error Correction Type</source>
        <translation>Тип виправлення помилок</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="206"/>
        <source>Maximum Capacity</source>
        <translation>Максимальна місткість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="207"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="65"/>
        <source>Error Information Handle</source>
        <translation>Обробник відомостей щодо помилок</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="208"/>
        <source>Number Of Devices</source>
        <translation>Кількість пристроїв</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="210"/>
        <source>BIOS ROMSIZE</source>
        <translation>Розмір BIOS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="211"/>
        <source>Release date</source>
        <translation>Дата випуску</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="212"/>
        <source>Board name</source>
        <translation>Назва плати</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="221"/>
        <source>SMBIOS Version</source>
        <translation>Версія SMBIOS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="223"/>
        <source>Language Description Format</source>
        <translation>Формат опису мови</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="224"/>
        <source>Installable Languages</source>
        <translation>Придатні до встановлення мови</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="225"/>
        <source>Currently Installed Language</source>
        <translation>Поточна встановлена мова</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="183"/>
        <source>BD Address</source>
        <translation>Адреса BD</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="184"/>
        <source>ACL MTU</source>
        <translation>MTU ACL</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="185"/>
        <source>SCO MTU</source>
        <translation>MTU SCO</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="187"/>
        <source>Packet type</source>
        <translation>Тип пакета</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="188"/>
        <source>Link policy</source>
        <translation>Правила зв&apos;язку</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="189"/>
        <source>Link mode</source>
        <translation>Режим зв&apos;язку</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="190"/>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="204"/>
        <source>Class</source>
        <translation>Клас</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="191"/>
        <source>Service Classes</source>
        <translation>Класи служби</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="192"/>
        <source>Device Class</source>
        <translation>Клас пристрою</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="193"/>
        <source>HCI Version</source>
        <translation>Версія HCI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="195"/>
        <source>LMP Version</source>
        <translation>Версія LMP</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="196"/>
        <source>Subversion</source>
        <translation>Підверсія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="198"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="35"/>
        <source>Device</source>
        <translation>Пристрій</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="199"/>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="134"/>
        <source>Serial ID</source>
        <translation>Послідовний ід.</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="201"/>
        <source>product</source>
        <translation>продукт</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="202"/>
        <source>description</source>
        <translation>опис</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="205"/>
        <source>Powered</source>
        <translation>Живлення</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="206"/>
        <source>Discoverable</source>
        <translation>Може визначатися</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="207"/>
        <source>Pairable</source>
        <translation>Може пов&apos;язуватися</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="209"/>
        <source>Modalias</source>
        <translation>Мод-прив&apos;язка</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="210"/>
        <source>Discovering</source>
        <translation>Визначення</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="136"/>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="202"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="372"/>
        <source>Device File</source>
        <translation>Файл пристрою</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="137"/>
        <source>Device Files</source>
        <translation>Файли пристроїв</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="138"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="381"/>
        <source>Device Number</source>
        <translation>Номер пристрою</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="139"/>
        <source>Module Alias</source>
        <translation>Альт. назва модуля</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="141"/>
        <source>Application</source>
        <translation>Програма</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="144"/>
        <source>status</source>
        <translation>стан</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="145"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="375"/>
        <source>logical name</source>
        <translation>логічна назва</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="147"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="373"/>
        <source>ansiversion</source>
        <translation>версія ANSI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="34"/>
        <source>CPU implementer</source>
        <translation>Реалізатор процесора</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="35"/>
        <source>CPU architecture</source>
        <translation>Архітектура процесора</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="36"/>
        <source>CPU variant</source>
        <translation>Варіант процесора</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="37"/>
        <source>CPU part</source>
        <translation>Частина процесора</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="38"/>
        <source>CPU revision</source>
        <translation>Модифікація процесора</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="355"/>
        <source>One</source>
        <translation>Одне</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="356"/>
        <source>Two</source>
        <translation>Два</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="357"/>
        <source>Four</source>
        <translation>Чотири</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="358"/>
        <source>Six</source>
        <translation>Шістка</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="359"/>
        <source>Eight</source>
        <translation>Вісім</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="361"/>
        <source>Ten</source>
        <translation>Десятка</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="362"/>
        <source>Twelve</source>
        <translation>Дванадцять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="363"/>
        <source>Fourteen</source>
        <translation>Чотирнадцять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="364"/>
        <source>Sixteen</source>
        <translation>Шістнадцять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="365"/>
        <source>Eighteen</source>
        <translation>Вісімнадцять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="367"/>
        <source>Twenty</source>
        <translation>Двадцять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="368"/>
        <source>Twenty-two</source>
        <translation>Двадцять два</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="369"/>
        <source>Twenty-four</source>
        <translation>Двадцять чотири</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="370"/>
        <source>Twenty-six</source>
        <translation>Двадцять шість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="371"/>
        <source>Twenty-eight</source>
        <translation>Двадцять вісім</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="373"/>
        <source>Thirty</source>
        <translation>Тридцять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="374"/>
        <source>Thirty-two</source>
        <translation>Тридцять два</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="375"/>
        <source>Thirty-four</source>
        <translation>Тридцять чотири</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="376"/>
        <source>Thirty-six</source>
        <translation>Тридцять шість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="377"/>
        <source>Thirty-eight</source>
        <translation>Тридцять вісім</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="379"/>
        <source>Forty</source>
        <translation>Сорок</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="380"/>
        <source>Forty-two</source>
        <translation>Сорок два</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="381"/>
        <source>Forty-four</source>
        <translation>Сорок чотири</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="382"/>
        <source>Forty-six</source>
        <translation>Сорок шість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="383"/>
        <source>Forty-eight</source>
        <translation>Сорок вісім</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="385"/>
        <source>Fifty</source>
        <translation>П’ятдесят</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="386"/>
        <source>Fifty-two</source>
        <translation>П&apos;ятдесят два</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="387"/>
        <source>Fifty-four</source>
        <translation>П&apos;ятдесят чотири</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="388"/>
        <source>Fifty-six</source>
        <translation>П&apos;ятдесят шість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="389"/>
        <source>Fifty-eight</source>
        <translation>П&apos;ятдесят вісім</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="391"/>
        <source>Sixty</source>
        <translation>Шістдесят</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="392"/>
        <source>Sixty-two</source>
        <translation>Шістдесят два</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="393"/>
        <source>Sixty-four</source>
        <translation>Шістдесят чотири</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="394"/>
        <source>Sixty-six</source>
        <translation>Шістдесят шість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="395"/>
        <source>Sixty-eight</source>
        <translation>Шістдесят вісім</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="397"/>
        <source>Seventy</source>
        <translation>Сімдесят</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="398"/>
        <source>Seventy-two</source>
        <translation>Сімдесят два</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="399"/>
        <source>Seventy-four</source>
        <translation>Сімдесят чотири</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="400"/>
        <source>Seventy-six</source>
        <translation>Сімдесят шість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="401"/>
        <source>Seventy-eight</source>
        <translation>Сімдесят вісім</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="403"/>
        <source>Eighty</source>
        <translation>Вісімдесят</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="404"/>
        <source>Eighty-two</source>
        <translation>Вісімдесят два</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="405"/>
        <source>Eighty-four</source>
        <translation>Вісімдесят чотири</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="406"/>
        <source>Eighty-six</source>
        <translation>Вісімдесят шість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="407"/>
        <source>Eighty-eight</source>
        <translation>Вісімдесят вісім</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="409"/>
        <source>Ninety</source>
        <translation>Дев&apos;яносто</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="410"/>
        <source>Ninety-two</source>
        <translation>Дев&apos;яносто два</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="411"/>
        <source>Ninety-four</source>
        <translation>Дев&apos;яносто чотири</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="412"/>
        <source>Ninety-six</source>
        <translation>Дев&apos;яносто шість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="413"/>
        <source>Ninety-eight</source>
        <translation>Дев&apos;яносто вісім</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="415"/>
        <source>One hundred</source>
        <translation>Сто</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="416"/>
        <source>One hundred and Two</source>
        <translation>Сто два</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="417"/>
        <source>One hundred and four</source>
        <translation>Сто чотири</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="418"/>
        <source>One hundred and Six</source>
        <translation>Сто шість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="419"/>
        <source>One hundred and Eight</source>
        <translation>Сто вісім</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="421"/>
        <source>One hundred and Ten</source>
        <translation>Сто десять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="422"/>
        <source>One hundred and Twelve</source>
        <translation>Сто дванадцять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="423"/>
        <source>One hundred and Fourteen</source>
        <translation>Сто чотирнадцять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="424"/>
        <source>One hundred and Sixteen</source>
        <translation>Сто шістнадцять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="425"/>
        <source>One hundred and Eighteen</source>
        <translation>Сто вісімнадцять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="427"/>
        <source>One hundred and Twenty</source>
        <translation>Сто двадцять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="428"/>
        <source>One hundred and Twenty-two</source>
        <translation>Сто двадцять два</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="429"/>
        <source>One hundred and Twenty-four</source>
        <translation>Сто двадцять чотири</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="430"/>
        <source>One hundred and Twenty-six</source>
        <translation>Сто двадцять шість</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="431"/>
        <source>One hundred and Twenty-eight</source>
        <translation>Сто двадцять вісім</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="43"/>
        <source>GDDR capacity</source>
        <translation>Місткість GDDR</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="44"/>
        <source>GPU vendor</source>
        <translation>Виробник графічного процесора</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="45"/>
        <source>GPU type</source>
        <translation>Тип графічного процесора</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="46"/>
        <source>EGL version</source>
        <translation>Версія EGL</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="47"/>
        <source>EGL client APIs</source>
        <translation>Клієнтські програмні інтерфейси EGL</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="48"/>
        <source>GL version</source>
        <translation>Версія GL</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="49"/>
        <source>GLSL version</source>
        <translation>Версія GLSL</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInfo.cpp" line="69"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="183"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="204"/>
        <source>Unknown</source>
        <translation>Невідомий</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="201"/>
        <source>MSC</source>
        <translation>MSC</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="203"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="371"/>
        <source>Hardware Class</source>
        <translation>Клас обладнання</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="698"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="725"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="752"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="787"/>
        <source>CPU</source>
        <translation>Процесор</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="698"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="725"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="752"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="787"/>
        <source>No CPU found</source>
        <translation>Процесорів не знайдено</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="699"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="726"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="753"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="788"/>
        <source>Motherboard</source>
        <translation>Материнська плата</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="699"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="726"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="753"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="788"/>
        <source>No motherboard found</source>
        <translation>Материнської плати не знайдено</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="700"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="727"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="754"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="789"/>
        <source>Memory</source>
        <translation>Пам&apos;ять</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="700"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="727"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="754"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="789"/>
        <source>No memory found</source>
        <translation>Пам&apos;яті не знайдено</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="701"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="728"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="755"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="790"/>
        <source>Storage</source>
        <translation>Сховище даних</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="701"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="728"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="755"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="790"/>
        <source>No disk found</source>
        <translation>Не знайдено дисків</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="702"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="729"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="756"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="791"/>
        <source>Display Adapter</source>
        <translation>Адаптер дисплея</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="702"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="729"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="756"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="791"/>
        <source>No GPU found</source>
        <translation>Графічних процесорів не виявлено</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="703"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="730"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="757"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="792"/>
        <source>Monitor</source>
        <translation>Монітор</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="703"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="730"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="757"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="792"/>
        <source>No monitor found</source>
        <translation>Моніторів не знайдено</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="704"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="731"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="758"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="793"/>
        <source>Network Adapter</source>
        <translation>Адаптер мережі</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="704"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="731"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="758"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="793"/>
        <source>No network adapter found</source>
        <translation>Не знайдено адаптера мережі</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="705"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="732"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="759"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="794"/>
        <source>Sound Adapter</source>
        <translation>Звуковий адаптер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="705"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="732"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="759"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="794"/>
        <source>No audio device found</source>
        <translation>Звукових пристроїв не знайдено</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="706"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="733"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="760"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="795"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="706"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="733"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="760"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="795"/>
        <source>No Bluetooth device found</source>
        <translation>Не знайдено пристроїв Bluetooth</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="707"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="734"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="761"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="796"/>
        <source>Other PCI Devices</source>
        <translation>Інші пристрої PCI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="707"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="734"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="761"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="796"/>
        <source>No other PCI devices found</source>
        <translation>Не знайдено інших пристроїв PCI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="708"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="735"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="762"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="797"/>
        <source>Power</source>
        <translation>Живлення</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="708"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="735"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="762"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="797"/>
        <source>No battery found</source>
        <translation>Не знайдено акумулятора</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="709"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="736"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="763"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="798"/>
        <source>Keyboard</source>
        <translation>Клавіатура</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="709"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="736"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="763"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="798"/>
        <source>No keyboard found</source>
        <translation>Не знайдено клавіатури</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="710"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="737"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="764"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="799"/>
        <source>Mouse</source>
        <translation>Миша</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="710"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="737"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="764"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="799"/>
        <source>No mouse found</source>
        <translation>Не знайдено миші</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="711"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="738"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="765"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="800"/>
        <source>Printer</source>
        <translation>Принтер</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="711"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="738"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="765"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="800"/>
        <source>No printer found</source>
        <translation>Не знайдено принтерів</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="712"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="739"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="766"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="801"/>
        <source>Camera</source>
        <translation>Камера</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="712"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="739"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="766"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="801"/>
        <source>No camera found</source>
        <translation>Не знайдено камери</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="713"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="740"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="767"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="802"/>
        <source>CD-ROM</source>
        <translation>CD-ROM</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="713"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="740"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="767"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="802"/>
        <source>No CD-ROM found</source>
        <translation>Не знайдено CD-ROM</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="714"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="741"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="768"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="803"/>
        <source>Other Devices</source>
        <translation>Інші пристрої</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="714"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="741"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="768"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="803"/>
        <source>No other devices found</source>
        <translation>Інших пристроїв не знайдено</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="64"/>
        <source>Array Handle</source>
        <translation>Обробник масивів</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="66"/>
        <source>Form Factor</source>
        <translation>Формфактор</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="67"/>
        <source>Set</source>
        <translation>Набір</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="68"/>
        <source>Bank Locator</source>
        <translation>Локатор банків</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="69"/>
        <source>Type Detail</source>
        <translation>Подробиці щодо типу</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="71"/>
        <source>Part Number</source>
        <translation>Номер частини</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="72"/>
        <source>Rank</source>
        <translation>Ранг</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="73"/>
        <source>Memory Technology</source>
        <translation>Технологія пам&apos;яті</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="74"/>
        <source>Memory Operating Mode Capability</source>
        <translation>Можливості режиму керування пам&apos;яттю</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="75"/>
        <source>Firmware Version</source>
        <translation>Версія мікропрограми</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="76"/>
        <source>Module Manufacturer ID</source>
        <translation>Ідентифікатор виробника модуля</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="77"/>
        <source>Module Product ID</source>
        <translation>Ідентифікатор продукту модуля</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="78"/>
        <source>Memory Subsystem Controller Manufacturer ID</source>
        <translation>Ідентифікатор виробника контролера підсистеми пам&apos;яті</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="79"/>
        <source>Memory Subsystem Controller Product ID</source>
        <translation>Ідентифікатор продукту контролера підсистеми пам&apos;яті</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="80"/>
        <source>Non-Volatile Size</source>
        <translation>Сталий розмір</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="81"/>
        <source>Volatile Size</source>
        <translation>Мінливий розмір</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="82"/>
        <source>Cache Size</source>
        <translation>Розмір кешу</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="83"/>
        <source>Logical Size</source>
        <translation>Логічний розмір</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="43"/>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="56"/>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="382"/>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="409"/>
        <source>inch</source>
        <translation>дюйм</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="260"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="227"/>
        <source>ioport</source>
        <translation>Порт ВВ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="229"/>
        <source>network</source>
        <translation>мережа</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="38"/>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="64"/>
        <source>battery</source>
        <translation>акумулятор</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="181"/>
        <source>native-path</source>
        <translation>природний шлях</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="182"/>
        <source>power supply</source>
        <translation>живлення</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="183"/>
        <source>updated</source>
        <translation>оновлено</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="184"/>
        <source>has history</source>
        <translation>має журнал</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="185"/>
        <source>has statistics</source>
        <translation>має статистику</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="186"/>
        <source>rechargeable</source>
        <translation>перезарядка</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="187"/>
        <source>state</source>
        <translation>стан</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="188"/>
        <source>warning-level</source>
        <translation>рівень попередження</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="189"/>
        <source>energy</source>
        <translation>заряд</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="190"/>
        <source>energy-empty</source>
        <translation>порожній</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="191"/>
        <source>energy-full</source>
        <translation>повний заряд</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="192"/>
        <source>energy-full-design</source>
        <translation>проектний повний заряд</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="193"/>
        <source>energy-rate</source>
        <translation>частка заряду</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="194"/>
        <source>voltage</source>
        <translation>напруга</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="195"/>
        <source>percentage</source>
        <translation>частка</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="196"/>
        <source>temperature</source>
        <translation>температура</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="197"/>
        <source>technology</source>
        <translation>технологія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="198"/>
        <source>icon-name</source>
        <translation>назва піктограми</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="199"/>
        <source>online</source>
        <translation>задіяно</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="200"/>
        <source>daemon-version</source>
        <translation>версія фонової служби</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="201"/>
        <source>on-battery</source>
        <translation>на акумуляторі</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="202"/>
        <source>lid-is-closed</source>
        <translation>кришку закрито</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="203"/>
        <source>lid-is-present</source>
        <translation>кришка є</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="204"/>
        <source>critical-action</source>
        <translation>критична дія</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="116"/>
        <source>copies</source>
        <translation>копії</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="119"/>
        <source>job-cancel-after</source>
        <translation>завдання скасовано після</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="120"/>
        <source>job-hold-until</source>
        <translation>затримання завдання до</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="121"/>
        <source>job-priority</source>
        <translation>пріоритетність завдання</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="123"/>
        <source>marker-change-time</source>
        <translation>час зміни маркера</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="126"/>
        <source>number-up</source>
        <translation>номер згори</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="127"/>
        <source>orientation-requested</source>
        <translation>потрібна орієнтація</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="128"/>
        <source>print-color-mode</source>
        <translation>режим колірності друку</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="132"/>
        <source>printer-is-accepting-jobs</source>
        <translation>принтер приймає завдання</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="133"/>
        <source>printer-is-shared</source>
        <translation>принтер є спільним</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="134"/>
        <source>printer-is-temporary</source>
        <translation>принтер є тимчасовим</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="136"/>
        <source>printer-make-and-model</source>
        <translation>виробник і модель принтера</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="138"/>
        <source>printer-state-change-time</source>
        <translation>час зміни стану принтера</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="139"/>
        <source>printer-state-reasons</source>
        <translation>причини стану принтера</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="140"/>
        <source>printer-type</source>
        <translation>тип принтера</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="141"/>
        <source>printer-uri-supported</source>
        <translation>підтримка адреси принтера</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="142"/>
        <source>sides</source>
        <translation>боки</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="179"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="200"/>
        <source>SSD</source>
        <translation>SSD</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="181"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="202"/>
        <source>HDD</source>
        <translation>HDD</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="374"/>
        <source>bus info</source>
        <translation>дані щодо каналу</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="376"/>
        <source>logicalsectorsize</source>
        <translation>розмір логічного сектора</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="378"/>
        <source>sectorsize</source>
        <translation>розмір сектора</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="379"/>
        <source>guid</source>
        <translation>guid</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="382"/>
        <source>Geometry (Logical)</source>
        <translation>Геометрія (логічна)</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="46"/>
        <location filename="../src/main.cpp" line="50"/>
        <source>Device Manager</source>
        <translation>Керування пристроями</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="52"/>
        <source>Device Manager is a handy tool for viewing hardware information and managing the devices.</source>
        <translation>«Керування пристроями» — зручний інструмент для перегляду відомостей щодо обладнання та керування пристроями.</translation>
    </message>
</context>
<context>
    <name>TableWidget</name>
    <message>
        <location filename="../src/Widget/TableWidget.cpp" line="23"/>
        <location filename="../src/Widget/TableWidget.cpp" line="135"/>
        <source>Disable</source>
        <translation>Вимкнути</translation>
    </message>
    <message>
        <location filename="../src/Widget/TableWidget.cpp" line="24"/>
        <source>Refresh</source>
        <translation>Оновити</translation>
    </message>
    <message>
        <location filename="../src/Widget/TableWidget.cpp" line="25"/>
        <source>Export</source>
        <translation>Експортувати</translation>
    </message>
    <message>
        <location filename="../src/Widget/TableWidget.cpp" line="137"/>
        <location filename="../src/Widget/TableWidget.cpp" line="167"/>
        <source>Enable</source>
        <translation>Увімкнути</translation>
    </message>
</context>
<context>
    <name>TextBrowser</name>
    <message>
        <location filename="../src/Widget/TextBrowser.cpp" line="19"/>
        <source>Refresh</source>
        <translation>Оновити</translation>
    </message>
    <message>
        <location filename="../src/Widget/TextBrowser.cpp" line="20"/>
        <source>Export</source>
        <translation>Експортувати</translation>
    </message>
    <message>
        <location filename="../src/Widget/TextBrowser.cpp" line="21"/>
        <source>Copy</source>
        <translation>Копіювати</translation>
    </message>
    <message>
        <location filename="../src/Widget/TextBrowser.cpp" line="254"/>
        <source>Disable</source>
        <translation>Вимкнути</translation>
    </message>
</context>
<context>
    <name>ThreadPool</name>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="218"/>
        <source>Loading Audio Device Info...</source>
        <translation>Завантажуємо дані щодо звукових пристроїв…</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="221"/>
        <source>Loading BIOS Info...</source>
        <translation>Завантажуємо дані BIOS...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="231"/>
        <source>Loading CD-ROM Info...</source>
        <translation>Завантажуємо дані щодо CD-ROM...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="235"/>
        <source>Loading Bluetooth Device Info...</source>
        <translation>Завантажуємо дані щодо пристроїв Bluetooth…</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="236"/>
        <source>Loading Image Devices Info...</source>
        <translation>Завантажуємо дані щодо графічних пристроїв…</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="238"/>
        <source>Loading Keyboard Info...</source>
        <translation>Завантажуємо дані щодо клавіатури…</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="244"/>
        <source>Loading Operating System Info...</source>
        <translation>Завантажуємо дані щодо операційної системи…</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="246"/>
        <source>Loading CPU Info...</source>
        <translation>Завантажуємо відомості щодо процесора…</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="247"/>
        <source>Loading Other Devices Info...</source>
        <translation>Завантажуємо дані щодо інших пристроїв…</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="248"/>
        <source>Loading Power Info...</source>
        <translation>Завантажуємо дані щодо живлення…</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="249"/>
        <source>Loading Printer Info...</source>
        <translation>Завантажуємо дані щодо принтерів…</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="256"/>
        <source>Loading Monitor Info...</source>
        <translation>Завантажуємо дані щодо монітора…</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="257"/>
        <source>Loading Mouse Info...</source>
        <translation>Завантажуємо дані щодо миші…</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="258"/>
        <source>Loading Network Adapter Info...</source>
        <translation>Завантажуємо дані щодо адаптера мережі…</translation>
    </message>
</context>
<context>
    <name>WaitingWidget</name>
    <message>
        <location filename="../src/Page/WaitingWidget.cpp" line="15"/>
        <source>Loading...</source>
        <translation>Завантаження…</translation>
    </message>
</context>
</TS>