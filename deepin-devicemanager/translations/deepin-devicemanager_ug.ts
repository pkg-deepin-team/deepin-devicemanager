<?xml version="1.0" ?><!DOCTYPE TS><TS language="ug" version="2.1">
<context>
    <name>CmdButtonWidget</name>
    <message>
        <location filename="../src/Widget/CmdButtonWidget.cpp" line="14"/>
        <source>More</source>
        <translation>تېخىمۇ كۆپ</translation>
    </message>
</context>
<context>
    <name>DetailButton</name>
    <message>
        <location filename="../src/Page/PageDetail.cpp" line="33"/>
        <location filename="../src/Page/PageDetail.cpp" line="36"/>
        <source>More</source>
        <translation>تېخىمۇ كۆپ</translation>
    </message>
    <message>
        <location filename="../src/Page/PageDetail.cpp" line="34"/>
        <source>Collapse</source>
        <translation>يىمىرىلىش</translation>
    </message>
</context>
<context>
    <name>DetailTreeView</name>
    <message>
        <location filename="../src/Widget/DetailTreeView.cpp" line="83"/>
        <location filename="../src/Widget/DetailTreeView.cpp" line="249"/>
        <source>More</source>
        <translation>تېخىمۇ كۆپ</translation>
    </message>
    <message>
        <location filename="../src/Widget/DetailTreeView.cpp" line="255"/>
        <source>Collapse</source>
        <translation>يىمىرىلىش</translation>
    </message>
</context>
<context>
    <name>DetailViewDelegate</name>
    <message>
        <location filename="../src/Widget/DetailViewDelegate.cpp" line="226"/>
        <location filename="../src/Widget/DetailViewDelegate.cpp" line="230"/>
        <source>Disable</source>
        <translation>چەكلەش</translation>
    </message>
</context>
<context>
    <name>DeviceAudio</name>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="221"/>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="299"/>
        <source>Disable</source>
        <translation>چەكلەش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="237"/>
        <source>Device Name</source>
        <translation>ئۈسكۈنىنىڭ ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="266"/>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="290"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="267"/>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="291"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="268"/>
        <source>Model</source>
        <translation>مودىل</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="269"/>
        <source>Version</source>
        <translation>نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="270"/>
        <source>Bus Info</source>
        <translation>باس ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="276"/>
        <source>Chip</source>
        <translation>ئۆزەك</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="277"/>
        <source>Capabilities</source>
        <translation>ئىقتىدارى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="278"/>
        <source>Clock</source>
        <translation>سائەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="279"/>
        <source>Width</source>
        <translation>كەڭلىك</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="280"/>
        <source>Memory</source>
        <translation>ساقلىغۇ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="281"/>
        <source>IRQ</source>
        <translation>IRQ</translation>
    </message>
</context>
<context>
    <name>DeviceBaseInfo</name>
    <message>
        <location filename="../src/DeviceManager/DeviceInfo.cpp" line="441"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInfo.cpp" line="442"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInfo.cpp" line="443"/>
        <source>Model</source>
        <translation>مودىل</translation>
    </message>
</context>
<context>
    <name>DeviceBios</name>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="231"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="232"/>
        <source>Version</source>
        <translation>نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="233"/>
        <source>Chipset</source>
        <translation>ئۆزەك گورۇپىسى</translation>
    </message>
</context>
<context>
    <name>DeviceBluetooth</name>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="216"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="217"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="218"/>
        <source>Version</source>
        <translation>نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="219"/>
        <source>Model</source>
        <translation>مودىل</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="244"/>
        <source>Speed</source>
        <translation>سۈرئەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="245"/>
        <source>Maximum Power</source>
        <translation>ئەڭ چوڭ قۇۋۋەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="246"/>
        <source>Driver Version</source>
        <translation>قوزغاتقۇچ نەشىرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="247"/>
        <source>Driver</source>
        <translation>قوزغاتقۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="248"/>
        <source>Capabilities</source>
        <translation>ئىقتىدارى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="249"/>
        <source>Bus Info</source>
        <translation>باس ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="250"/>
        <source>Logical Name</source>
        <translation>لوگىكىلىق ئىسىم</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="251"/>
        <source>MAC Address</source>
        <translation>MAC ئادرېسى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="262"/>
        <source>Disable</source>
        <translation>چەكلەش</translation>
    </message>
</context>
<context>
    <name>DeviceCdrom</name>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="153"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="154"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="155"/>
        <source>Model</source>
        <translation>مودىل</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="156"/>
        <source>Version</source>
        <translation>نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="157"/>
        <source>Bus Info</source>
        <translation>باس ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="158"/>
        <source>Capabilities</source>
        <translation>ئىقتىدارى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="159"/>
        <source>Driver</source>
        <translation>قوزغاتقۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="160"/>
        <source>Maximum Power</source>
        <translation>ئەڭ چوڭ قۇۋۋەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="161"/>
        <source>Speed</source>
        <translation>سۈرئەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="175"/>
        <source>Disable</source>
        <translation>چەكلەش</translation>
    </message>
</context>
<context>
    <name>DeviceComputer</name>
    <message>
        <location filename="../src/DeviceManager/DeviceComputer.cpp" line="165"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
</context>
<context>
    <name>DeviceCpu</name>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="44"/>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="337"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="45"/>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="338"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="46"/>
        <source>CPU ID</source>
        <translation>CPU كىملىك</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="47"/>
        <source>Core ID</source>
        <translation>يادرولۇق كىملىك</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="48"/>
        <source>Threads</source>
        <translation>تېما</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="49"/>
        <source>Current Speed</source>
        <translation>نۆۋەتتىكى سۈرئەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="50"/>
        <source>BogoMIPS</source>
        <translation>BogoMIPS</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="51"/>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="340"/>
        <source>Architecture</source>
        <translation>قۇرۇلما</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="52"/>
        <source>CPU Family</source>
        <translation>CPU ئائىلىسى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="53"/>
        <source>Model</source>
        <translation>مودىل</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="185"/>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="197"/>
        <source>Processor</source>
        <translation>بىر تەرەپ قىلغۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="195"/>
        <source>Core(s)</source>
        <translation>Core (s)</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="321"/>
        <source>Virtualization</source>
        <translation>مەۋھۇملاشتۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="322"/>
        <source>Flags</source>
        <translation>بايراق</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="323"/>
        <source>Extensions</source>
        <translation>كېڭەيتىلمە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="324"/>
        <source>L3 Cache</source>
        <translation>L3 Cache</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="325"/>
        <source>L2 Cache</source>
        <translation>L2 Cache</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="326"/>
        <source>L1i Cache</source>
        <translation>L1i Cache</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="327"/>
        <source>L1d Cache</source>
        <translation>L1d Cache</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="328"/>
        <source>Stepping</source>
        <translation>قەدەم بېسىش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="339"/>
        <source>Speed</source>
        <translation>سۈرئەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="339"/>
        <source>Max Speed</source>
        <translation>چوققا سۈرئىتى</translation>
    </message>
</context>
<context>
    <name>DeviceGpu</name>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="55"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="56"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="57"/>
        <source>Model</source>
        <translation>مودىل</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="58"/>
        <source>Version</source>
        <translation>نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="59"/>
        <source>Graphics Memory</source>
        <translation>گرافىك ئەستە ساقلاش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="313"/>
        <source>Physical ID</source>
        <translation>فىزىكىلىق كىملىك</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="314"/>
        <source>Memory</source>
        <translation>ساقلىغۇ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="315"/>
        <source>IO Port</source>
        <translation>IO Port</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="316"/>
        <source>Bus Info</source>
        <translation>باس ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="317"/>
        <source>Maximum Resolution</source>
        <translation>ئەڭ چوڭ ئېنىقلىق دەرىجىسى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="318"/>
        <source>Minimum Resolution</source>
        <translation>ئەڭ تۆۋەن ئېنىقلىق</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="319"/>
        <source>Current Resolution</source>
        <translation>نۆۋەتتىكى قارار</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="320"/>
        <source>Driver</source>
        <translation>قوزغاتقۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="321"/>
        <source>Description</source>
        <translation>چۈشەندۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="322"/>
        <source>Clock</source>
        <translation>سائەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="323"/>
        <source>DP</source>
        <translation>DP</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="324"/>
        <source>eDP</source>
        <translation>eDP</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="325"/>
        <source>HDMI</source>
        <translation>HDMI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="326"/>
        <source>VGA</source>
        <translation>VGA</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="327"/>
        <source>DVI</source>
        <translation>DVI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="328"/>
        <source>Display Output</source>
        <translation>چىقىرىشنى كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="329"/>
        <source>Capabilities</source>
        <translation>ئىقتىدارى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="330"/>
        <source>IRQ</source>
        <translation>IRQ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="331"/>
        <source>Width</source>
        <translation>كەڭلىك</translation>
    </message>
</context>
<context>
    <name>DeviceImage</name>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="148"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="149"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="150"/>
        <source>Version</source>
        <translation>نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="151"/>
        <source>Model</source>
        <translation>مودىل</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="152"/>
        <source>Bus Info</source>
        <translation>باس ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="158"/>
        <source>Speed</source>
        <translation>سۈرئەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="159"/>
        <source>Maximum Power</source>
        <translation>ئەڭ چوڭ قۇۋۋەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="160"/>
        <source>Driver</source>
        <translation>قوزغاتقۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="161"/>
        <source>Capabilities</source>
        <translation>ئىقتىدارى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceImage.cpp" line="172"/>
        <source>Disable</source>
        <translation>چەكلەش</translation>
    </message>
</context>
<context>
    <name>DeviceInput</name>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="211"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="212"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="213"/>
        <source>Model</source>
        <translation>مودىل</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="214"/>
        <source>Interface</source>
        <translation>كۆرۈنمە يۈزى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="215"/>
        <source>Bus Info</source>
        <translation>باس ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="221"/>
        <source>Speed</source>
        <translation>سۈرئەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="222"/>
        <source>Maximum Power</source>
        <translation>ئەڭ چوڭ قۇۋۋەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="223"/>
        <source>Driver</source>
        <translation>قوزغاتقۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="224"/>
        <source>Capabilities</source>
        <translation>ئىقتىدارى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="225"/>
        <source>Version</source>
        <translation>نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="235"/>
        <source>Disable</source>
        <translation>چەكلەش</translation>
    </message>
</context>
<context>
    <name>DeviceManager</name>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="136"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="190"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="820"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="839"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="859"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="867"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="882"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="892"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="907"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="918"/>
        <source>Overview</source>
        <translation>ئومۇمىي چۈشەنچە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="138"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="169"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="991"/>
        <source>CPU</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="141"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="170"/>
        <source>Motherboard</source>
        <translation>ئانا تاختا</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="142"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="171"/>
        <source>Memory</source>
        <translation>ساقلىغۇ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="143"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="172"/>
        <source>Display Adapter</source>
        <translation>ماسلاشتۇرغۇچنى كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="144"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="173"/>
        <source>Sound Adapter</source>
        <translation>ئاۋاز ماسلاشتۇرغۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="145"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="174"/>
        <source>Storage</source>
        <translation>ساقلاش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="146"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="175"/>
        <source>Other PCI Devices</source>
        <translation>باشقا PCI ئۈسكۈنىلىرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="147"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="176"/>
        <source>Battery</source>
        <translation>باتارېيە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="150"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="177"/>
        <source>Bluetooth</source>
        <translation>كۆك چىش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="151"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="178"/>
        <source>Network Adapter</source>
        <translation>تور ماسلاشتۇرغۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="154"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="179"/>
        <source>Mouse</source>
        <translation>چاشقىنەك</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="155"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="180"/>
        <source>Keyboard</source>
        <translation>كۇنۇپكا تاختىسى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="158"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="181"/>
        <source>Monitor</source>
        <translation>نازارەتچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="159"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="182"/>
        <source>CD-ROM</source>
        <translation>CD-ROM</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="160"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="183"/>
        <source>Printer</source>
        <translation>پرىنتېر</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="161"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="184"/>
        <source>Camera</source>
        <translation>كامېرا</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="162"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="185"/>
        <source>Other Devices</source>
        <comment>Other Input Devices</comment>
        <translation>باشقا ئۈسكۈنىلەر</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="825"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="885"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="912"/>
        <source>Device</source>
        <translation>ئۈسكۈنە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="832"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="888"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="914"/>
        <source>OS</source>
        <translation>OS</translation>
    </message>
</context>
<context>
    <name>DeviceMemory</name>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="89"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="115"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="90"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="116"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="91"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="119"/>
        <source>Size</source>
        <translation>چوڭلۇقى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="92"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="117"/>
        <source>Type</source>
        <translation>تىپ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="93"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="118"/>
        <source>Speed</source>
        <translation>سۈرئەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="94"/>
        <source>Total Width</source>
        <translation>ئومۇمىي كەڭلىكى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="95"/>
        <source>Locator</source>
        <translation>ئورۇن بەلگىلىگۈچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="96"/>
        <source>Serial Number</source>
        <translation>تەرتىپ نومۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="103"/>
        <source>Configured Voltage</source>
        <translation>تەڭشەلگەن توك بېسىمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="104"/>
        <source>Maximum Voltage</source>
        <translation>ئەڭ چوڭ توك بېسىمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="105"/>
        <source>Minimum Voltage</source>
        <translation>ئەڭ تۆۋەن توك بېسىمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="106"/>
        <source>Configured Speed</source>
        <translation>تەڭشەلگەن سۈرئەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="107"/>
        <source>Data Width</source>
        <translation>سان-سىپېر چوڭلۇقى</translation>
    </message>
</context>
<context>
    <name>DeviceMonitor</name>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="266"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="267"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="268"/>
        <source>Type</source>
        <translation>تىپ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="269"/>
        <source>Display Input</source>
        <translation>كىرگۈزۈشنى كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="270"/>
        <source>Interface Type</source>
        <translation>كۆرۈنمە يۈزى تىپى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="276"/>
        <source>Support Resolution</source>
        <translation>قوللاش قارارى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="277"/>
        <source>Current Resolution</source>
        <translation>نۆۋەتتىكى قارار</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="278"/>
        <source>Primary Monitor</source>
        <translation>دەسلەپكى كۆزەتكۈ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="279"/>
        <source>Display Ratio</source>
        <translation>كۆرسىتىش نىسبىتى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="280"/>
        <source>Size</source>
        <translation>چوڭلۇقى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="281"/>
        <source>Serial Number</source>
        <translation>تەرتىپ نومۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="282"/>
        <source>Product Date</source>
        <translation>مەھسۇلات ۋاقتى</translation>
    </message>
</context>
<context>
    <name>DeviceNetwork</name>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="235"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="236"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="237"/>
        <source>Type</source>
        <translation>تىپ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="238"/>
        <source>Version</source>
        <translation>نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="239"/>
        <source>Bus Info</source>
        <translation>باس ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="240"/>
        <source>Capabilities</source>
        <translation>ئىقتىدارى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="241"/>
        <source>Driver</source>
        <translation>قوزغاتقۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="242"/>
        <source>Driver Version</source>
        <translation>قوزغاتقۇچ نۇسخىسى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="248"/>
        <source>Capacity</source>
        <translation>سىغىمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="249"/>
        <source>Speed</source>
        <translation>سۈرئەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="250"/>
        <source>Port</source>
        <translation>پورت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="251"/>
        <source>Multicast</source>
        <translation>Multicast</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="252"/>
        <source>Link</source>
        <translation>ئۇلىنىش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="253"/>
        <source>Latency</source>
        <translation>Latency</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="254"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="255"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="256"/>
        <source>Duplex</source>
        <translation>Duplex</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="257"/>
        <source>Broadcast</source>
        <translation>Broadcast</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="258"/>
        <source>Auto Negotiation</source>
        <translation>ئاپتوماتىك سۆھبەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="259"/>
        <source>Clock</source>
        <translation>سائەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="260"/>
        <source>Width</source>
        <translation>كەڭلىك</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="261"/>
        <source>Memory</source>
        <translation>ساقلىغۇ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="262"/>
        <source>IRQ</source>
        <translation>IRQ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="263"/>
        <source>MAC Address</source>
        <translation>MAC ئادرېسى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="264"/>
        <source>Logical Name</source>
        <translation>لوگىكىلىق ئىسىم</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="274"/>
        <source>Disable</source>
        <translation>چەكلەش</translation>
    </message>
</context>
<context>
    <name>DeviceOtherPCI</name>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="111"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="112"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="113"/>
        <source>Model</source>
        <translation>مودىل</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="114"/>
        <source>Bus Info</source>
        <translation>باس ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="115"/>
        <source>Version</source>
        <translation>نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="121"/>
        <source>Input/Output</source>
        <translation>كىرگۈزۈش / چىقىرىش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="122"/>
        <source>Memory</source>
        <translation>ساقلىغۇ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="123"/>
        <source>IRQ</source>
        <translation>IRQ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="124"/>
        <source>Latency</source>
        <translation>Latency</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="125"/>
        <source>Clock</source>
        <translation>سائەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="126"/>
        <source>Width</source>
        <translation>كەڭلىك</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="127"/>
        <source>Driver</source>
        <translation>قوزغاتقۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOtherPCI.cpp" line="128"/>
        <source>Capabilities</source>
        <translation>ئىقتىدارى</translation>
    </message>
</context>
<context>
    <name>DeviceOthers</name>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="130"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="131"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="132"/>
        <source>Model</source>
        <translation>مودىل</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="133"/>
        <source>Version</source>
        <translation>نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="134"/>
        <source>Bus Info</source>
        <translation>باس ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="135"/>
        <source>Capabilities</source>
        <translation>ئىقتىدارى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="136"/>
        <source>Driver</source>
        <translation>قوزغاتقۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="137"/>
        <source>Maximum Power</source>
        <translation>ئەڭ چوڭ قۇۋۋەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceOthers.cpp" line="138"/>
        <source>Speed</source>
        <translation>سۈرئەت</translation>
    </message>
</context>
<context>
    <name>DevicePower</name>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="210"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="211"/>
        <source>Model</source>
        <translation>مودىل</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="212"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="213"/>
        <source>Serial Number</source>
        <translation>تەرتىپ نومۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="214"/>
        <source>Type</source>
        <translation>تىپ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="215"/>
        <source>Status</source>
        <translation>ھالەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="216"/>
        <source>Capacity</source>
        <translation>سىغىمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="217"/>
        <source>Voltage</source>
        <translation>توك بېسىمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="218"/>
        <source>Slot</source>
        <translation>Slot</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="219"/>
        <source>Design Capacity</source>
        <translation>لايىھىلەش ئىقتىدارى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="220"/>
        <source>Design Voltage</source>
        <translation>لايىھىلەش بېسىمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="221"/>
        <source>SBDS Version</source>
        <translation>SBDS نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="222"/>
        <source>SBDS Serial Number</source>
        <translation>SBDS تەرتىپ نومۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="223"/>
        <source>SBDS Manufacture Date</source>
        <translation>SBDS ئىشلەپچىقىرىش ۋاقتى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="224"/>
        <source>SBDS Chemistry</source>
        <translation>SBDS Chemistry</translation>
    </message>
</context>
<context>
    <name>DevicePrint</name>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="148"/>
        <source>Name</source>
        <translation>ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="149"/>
        <source>Model</source>
        <translation>مودىل</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="150"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="151"/>
        <source>Serial Number</source>
        <translation>تەرتىپ نومۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="157"/>
        <source>Shared</source>
        <translation>ئورتاقلاشقان</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="158"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="159"/>
        <source>Status</source>
        <translation>ھالەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="160"/>
        <source>Interface Type</source>
        <translation>كۆرۈنمە يۈزى تىپى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="170"/>
        <source>Disable</source>
        <translation>چەكلەش</translation>
    </message>
</context>
<context>
    <name>DeviceStorage</name>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="390"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="416"/>
        <source>Model</source>
        <translation>مودىل</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="391"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="417"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="392"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="418"/>
        <source>Media Type</source>
        <translation>Media Type</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="393"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="419"/>
        <source>Size</source>
        <translation>چوڭلۇقى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="394"/>
        <source>Version</source>
        <translation>نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="395"/>
        <source>Capabilities</source>
        <translation>ئىقتىدارى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="401"/>
        <source>Power Cycle Count</source>
        <translation>توك دەۋرىيلىكى سانى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="402"/>
        <source>Power On Hours</source>
        <translation>سائەتتىكى كۈچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="403"/>
        <source>Firmware Version</source>
        <translation>يۇمشاق دېتال نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="404"/>
        <source>Speed</source>
        <translation>سۈرئەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="405"/>
        <source>Description</source>
        <translation>چۈشەندۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="406"/>
        <source>Serial Number</source>
        <translation>تەرتىپ نومۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="407"/>
        <source>Interface</source>
        <translation>كۆرۈنمە يۈزى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="408"/>
        <source>Rotation Rate</source>
        <translation>ئايلىنىش نىسبىتى</translation>
    </message>
</context>
<context>
    <name>LogTreeView</name>
    <message>
        <location filename="../src/Widget/logtreeview.cpp" line="82"/>
        <location filename="../src/Widget/logtreeview.cpp" line="101"/>
        <location filename="../src/Widget/logtreeview.cpp" line="103"/>
        <source>Disable</source>
        <translation>چەكلەش</translation>
    </message>
</context>
<context>
    <name>LogViewItemDelegate</name>
    <message>
        <location filename="../src/Widget/logviewitemdelegate.cpp" line="158"/>
        <source>Disable</source>
        <translation>چەكلەش</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="101"/>
        <source>Device Info</source>
        <comment>export file's name</comment>
        <translation>ئۈسكۈنە ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="174"/>
        <source>Display shortcuts</source>
        <translation>تېزلەتمىلەرنى كۆرسەت</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="175"/>
        <source>Close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="176"/>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="177"/>
        <source>Copy</source>
        <translation>كۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="181"/>
        <source>System</source>
        <translation>سىستېما</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="188"/>
        <source>Export</source>
        <translation>ئېكسپورت</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="189"/>
        <source>Refresh</source>
        <translation>يېڭىلاش</translation>
    </message>
    <message>
        <location filename="../src/Page/MainWindow.cpp" line="193"/>
        <source>Device Manager</source>
        <translation>ئۈسكۈنە باشقۇرغۇچى</translation>
    </message>
</context>
<context>
    <name>PageDetail</name>
    <message>
        <location filename="../src/Page/PageDetail.cpp" line="284"/>
        <source>More</source>
        <translation>تېخىمۇ كۆپ</translation>
    </message>
</context>
<context>
    <name>PageListView</name>
    <message>
        <location filename="../src/Page/PageListView.cpp" line="14"/>
        <source>Refresh</source>
        <translation>يېڭىلاش</translation>
    </message>
    <message>
        <location filename="../src/Page/PageListView.cpp" line="15"/>
        <source>Export</source>
        <translation>ئېكسپورت</translation>
    </message>
    <message>
        <location filename="../src/Page/PageListView.cpp" line="17"/>
        <source>Overview</source>
        <translation>ئومۇمىي چۈشەنچە</translation>
    </message>
</context>
<context>
    <name>PageMultiInfo</name>
    <message>
        <location filename="../src/Page/PageMultiInfo.cpp" line="95"/>
        <source>Failed to enable the device</source>
        <translation>ئۈسكۈنىنى قوزغىتىش مەغلۇب بولدى</translation>
    </message>
    <message>
        <location filename="../src/Page/PageMultiInfo.cpp" line="97"/>
        <source>Failed to disable the device</source>
        <translation>ئۈسكۈنىنى چەكلىيەلمىدى</translation>
    </message>
</context>
<context>
    <name>PageOverview</name>
    <message>
        <location filename="../src/Page/PageOverview.cpp" line="27"/>
        <source>Refresh</source>
        <translation>يېڭىلاش</translation>
    </message>
    <message>
        <location filename="../src/Page/PageOverview.cpp" line="28"/>
        <source>Export</source>
        <translation>ئېكسپورت</translation>
    </message>
    <message>
        <location filename="../src/Page/PageOverview.cpp" line="29"/>
        <source>Copy</source>
        <translation>كۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/Page/PageOverview.cpp" line="61"/>
        <source>Overview</source>
        <translation>ئومۇمىي چۈشەنچە</translation>
    </message>
</context>
<context>
    <name>PageSingleInfo</name>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="21"/>
        <source>Refresh</source>
        <translation>يېڭىلاش</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="22"/>
        <source>Export</source>
        <translation>ئېكسپورت</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="23"/>
        <source>Copy</source>
        <translation>كۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="24"/>
        <location filename="../src/Page/PageSingleInfo.cpp" line="77"/>
        <location filename="../src/Page/PageSingleInfo.cpp" line="120"/>
        <location filename="../src/Page/PageSingleInfo.cpp" line="153"/>
        <source>Enable</source>
        <translation>قوزغىتىش</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="118"/>
        <location filename="../src/Page/PageSingleInfo.cpp" line="168"/>
        <source>Disable</source>
        <translation>چەكلەش</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="159"/>
        <source>Failed to disable the device</source>
        <translation>ئۈسكۈنىنى چەكلىيەلمىدى</translation>
    </message>
    <message>
        <location filename="../src/Page/PageSingleInfo.cpp" line="174"/>
        <source>Failed to enable the device</source>
        <translation>ئۈسكۈنىنى قوزغىتىش مەغلۇب بولدى</translation>
    </message>
</context>
<context>
    <name>PageTableHeader</name>
    <message>
        <location filename="../src/Page/PageTableHeader.cpp" line="61"/>
        <source>Disable</source>
        <translation>چەكلەش</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="238"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="36"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="384"/>
        <source>SubVendor</source>
        <translation>قوشۇمچە ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="239"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="37"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="383"/>
        <source>SubDevice</source>
        <translation>قوشۇمچى ئۈسكىنە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="240"/>
        <source>Driver</source>
        <translation>قوزغاتقۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="241"/>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="135"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="38"/>
        <source>Driver Modules</source>
        <translation>قوزغاتقۇچ مودۇلى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="242"/>
        <source>Driver Status</source>
        <translation>قوزغاتقۇ ھالىتى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="243"/>
        <source>Driver Activation Cmd</source>
        <translation>شوپۇر ئاكتىپلاش Cmd</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="244"/>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="140"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="39"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="380"/>
        <source>Config Status</source>
        <translation>ھالەتنى تەڭشەش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="247"/>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="203"/>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="142"/>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="205"/>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="228"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="377"/>
        <source>physical id</source>
        <translation>فىزىكىلىق id</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="248"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="40"/>
        <source>latency</source>
        <translation>كېچىكىش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="250"/>
        <source>Phys</source>
        <translation>Phys</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="251"/>
        <source>Sysfs</source>
        <translation>Sysfs</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="252"/>
        <source>Handlers</source>
        <translation>بېجىرگۈچىلەر</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="253"/>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="198"/>
        <source>PROP</source>
        <translation>PROP</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="254"/>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="199"/>
        <source>EV</source>
        <translation>EV</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="255"/>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="200"/>
        <source>KEY</source>
        <translation>KEY</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="257"/>
        <source>Model</source>
        <translation>مودىل</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="258"/>
        <source>Vendor</source>
        <translation>ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="259"/>
        <source>Version</source>
        <translation>نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceAudio.cpp" line="260"/>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="182"/>
        <source>Bus</source>
        <translation>باس</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="25"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="215"/>
        <source>BIOS Information</source>
        <translation>BIOS ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="47"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="216"/>
        <source>Base Board Information</source>
        <translation>Base Board Information</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="69"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="217"/>
        <source>System Information</source>
        <translation>سىستېما ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="85"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="218"/>
        <source>Chassis Information</source>
        <translation>تەگلىك ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="101"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="219"/>
        <source>Physical Memory Array</source>
        <translation>فىزىكىلىق ساقلىغۇچ گورۇپىسى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="163"/>
        <source>Release Date</source>
        <translation>ئېلان قىلىنغان ۋاقىت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="164"/>
        <source>Address</source>
        <translation>ئادرېس</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="165"/>
        <source>Runtime Size</source>
        <translation>ئىجرا ۋاقتى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="166"/>
        <source>ROM Size</source>
        <translation>ROM Size</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="167"/>
        <source>Characteristics</source>
        <translation>ئالاھىدىلىكى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="168"/>
        <source>BIOS Revision</source>
        <translation>BIOS تۈزىتىلگەن نۇسخىسى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="169"/>
        <source>Firmware Revision</source>
        <translation>زاپچاس يامىقى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="171"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="180"/>
        <source>Product Name</source>
        <translation>مەھسۇلات ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="172"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="182"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="191"/>
        <source>Serial Number</source>
        <translation>تەرتىپ نومۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="173"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="192"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="70"/>
        <source>Asset Tag</source>
        <translation>مۈلۈك بەلگىسى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="174"/>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="186"/>
        <source>Features</source>
        <translation>خاسلىقلار</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="175"/>
        <source>Location In Chassis</source>
        <translation>تەگلىك ئورنى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="176"/>
        <source>Chassis Handle</source>
        <translation>Chassis Handle</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="177"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="189"/>
        <source>Type</source>
        <translation>تىپ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="178"/>
        <source>Contained Object Handles</source>
        <translation>مەزمۇننى ئۆز ئىچىگە ئالغان</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="183"/>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="208"/>
        <source>UUID</source>
        <translation>UUID</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="184"/>
        <source>Wake-up Type</source>
        <translation>ئويغىنىش تىپى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="185"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="201"/>
        <source>SKU Number</source>
        <translation>SKU نومۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="186"/>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="213"/>
        <source>Family</source>
        <translation>ئائىلە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="190"/>
        <source>Lock</source>
        <translation>قۇلۇپ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="193"/>
        <source>Boot-up State</source>
        <translation>قوزغىتىش ھالىتى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="194"/>
        <source>Power Supply State</source>
        <translation>توك بىلەن تەمىنلەش ھالىتى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="195"/>
        <source>Thermal State</source>
        <translation>قىززىق ھالەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="196"/>
        <source>Security Status</source>
        <translation>بىخەتەرلىك ھالەتلىرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="197"/>
        <source>OEM Information</source>
        <translation>OEM ئۇچۇرلىرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="198"/>
        <source>Height</source>
        <translation>ئېگىزلىكى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="199"/>
        <source>Number Of Power Cords</source>
        <translation>توك سىمىنىڭ سانى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="200"/>
        <source>Contained Elements</source>
        <translation>ئۆز ئىچىگە ئېلىنغان ئېلمىنىتلار</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="203"/>
        <source>Location</source>
        <translation>ئورنى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="205"/>
        <source>Error Correction Type</source>
        <translation>خاتالىق تۈزىتىش تىپى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="206"/>
        <source>Maximum Capacity</source>
        <translation>ئەڭ چوڭ سىغىمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="207"/>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="65"/>
        <source>Error Information Handle</source>
        <translation>خاتالىق ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="208"/>
        <source>Number Of Devices</source>
        <translation>ئۈسكۈنىلەرنىڭ سانى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="210"/>
        <source>BIOS ROMSIZE</source>
        <translation>BIOS ROMSIZE</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="211"/>
        <source>Release date</source>
        <translation>ئېلان قىلىنغان ۋاقىت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="212"/>
        <source>Board name</source>
        <translation>مۇدىرىيەت ئىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="221"/>
        <source>SMBIOS Version</source>
        <translation>SMBIOS نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="223"/>
        <source>Language Description Format</source>
        <translation>تىل چۈشەندۈرۈش فورماتى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="224"/>
        <source>Installable Languages</source>
        <translation>قاچىلىغىلى بولىدىغان تىللار</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBios.cpp" line="225"/>
        <source>Currently Installed Language</source>
        <translation>ھازىر قاچىلانغان تىل</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="183"/>
        <source>BD Address</source>
        <translation>BD ئادرېسى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="184"/>
        <source>ACL MTU</source>
        <translation>ACL MTU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="185"/>
        <source>SCO MTU</source>
        <translation>SCO MTU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="187"/>
        <source>Packet type</source>
        <translation>بولاق تىپى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="188"/>
        <source>Link policy</source>
        <translation>ئۇلىنىش سىياسىتى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="189"/>
        <source>Link mode</source>
        <translation>ئۇلىنىش ھالىتى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="190"/>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="204"/>
        <source>Class</source>
        <translation>كىلاس</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="191"/>
        <source>Service Classes</source>
        <translation>مۇلازىمەت دەرسلىرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="192"/>
        <source>Device Class</source>
        <translation>ئۈسكۈنە سىنىپى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="193"/>
        <source>HCI Version</source>
        <translation>HCI نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="195"/>
        <source>LMP Version</source>
        <translation>LMP نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="196"/>
        <source>Subversion</source>
        <translation>قوشۇمچە نەشىرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="198"/>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="35"/>
        <source>Device</source>
        <translation>ئۈسكۈنە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="199"/>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="134"/>
        <source>Serial ID</source>
        <translation>Serial ID</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="201"/>
        <source>product</source>
        <translation>مەھسۇلات</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="202"/>
        <source>description</source>
        <translation>چۈشەندۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="205"/>
        <source>Powered</source>
        <translation>تۆھپىكار</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="206"/>
        <source>Discoverable</source>
        <translation>بايقاشقا بولىدۇ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="207"/>
        <source>Pairable</source>
        <translation>مۇۋاپىق</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="209"/>
        <source>Modalias</source>
        <translation>Modalias</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceBluetooth.cpp" line="210"/>
        <source>Discovering</source>
        <translation>بايقاش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="136"/>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="202"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="372"/>
        <source>Device File</source>
        <translation>ئۈسكۈنە ھۆججىتى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="137"/>
        <source>Device Files</source>
        <translation>ئۈسكۈنە ھۆججەتلىرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="138"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="381"/>
        <source>Device Number</source>
        <translation>ئۈسكۈنە نومۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="139"/>
        <source>Module Alias</source>
        <translation>مودېل لەقىمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="141"/>
        <source>Application</source>
        <translation>ئىلتىماس</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="144"/>
        <source>status</source>
        <translation>ھالەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="145"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="375"/>
        <source>logical name</source>
        <translation>لوگىكىلىق ئىسىم</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCdrom.cpp" line="147"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="373"/>
        <source>ansiversion</source>
        <translation>ansiversion</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="34"/>
        <source>CPU implementer</source>
        <translation>مەركىزى بىر تەرەپ قىلغۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="35"/>
        <source>CPU architecture</source>
        <translation>CPU قۇرۇلمىسى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="36"/>
        <source>CPU variant</source>
        <translation>مەركىزى بىر تەرەپ قىلغۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="37"/>
        <source>CPU part</source>
        <translation>CPU قىسمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="38"/>
        <source>CPU revision</source>
        <translation>CPU تۈزىتىلگەن نۇسخىسى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="355"/>
        <source>One</source>
        <translation>بىرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="356"/>
        <source>Two</source>
        <translation>ئىككى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="357"/>
        <source>Four</source>
        <translation>تۆت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="358"/>
        <source>Six</source>
        <translation>ئالتە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="359"/>
        <source>Eight</source>
        <translation>سەككىز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="361"/>
        <source>Ten</source>
        <translation>ئون</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="362"/>
        <source>Twelve</source>
        <translation>ئون ئىككى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="363"/>
        <source>Fourteen</source>
        <translation>ئون تۆت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="364"/>
        <source>Sixteen</source>
        <translation>ئون ئالتە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="365"/>
        <source>Eighteen</source>
        <translation>ئون سەككىز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="367"/>
        <source>Twenty</source>
        <translation>يىگىرمە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="368"/>
        <source>Twenty-two</source>
        <translation>يىگىرمە ئىككى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="369"/>
        <source>Twenty-four</source>
        <translation>يىگىرمە تۆت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="370"/>
        <source>Twenty-six</source>
        <translation>يىگىرمە ئالتە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="371"/>
        <source>Twenty-eight</source>
        <translation>يىگىرمە سەككىز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="373"/>
        <source>Thirty</source>
        <translation>ئوتتۇز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="374"/>
        <source>Thirty-two</source>
        <translation>ئوتتۇز ئىككى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="375"/>
        <source>Thirty-four</source>
        <translation>ئوتتۇز تۆت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="376"/>
        <source>Thirty-six</source>
        <translation>ئوتتۇز ئالتە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="377"/>
        <source>Thirty-eight</source>
        <translation>ئوتتۇز سەككىز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="379"/>
        <source>Forty</source>
        <translation>قىرىق</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="380"/>
        <source>Forty-two</source>
        <translation>قىرىق ئىككى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="381"/>
        <source>Forty-four</source>
        <translation>قىرىق تۆت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="382"/>
        <source>Forty-six</source>
        <translation>قىرىق ئالتە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="383"/>
        <source>Forty-eight</source>
        <translation>قىرىق سەككىز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="385"/>
        <source>Fifty</source>
        <translation>ئەللىك</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="386"/>
        <source>Fifty-two</source>
        <translation>ئەللىك ئىككى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="387"/>
        <source>Fifty-four</source>
        <translation>ئەللىك تۆت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="388"/>
        <source>Fifty-six</source>
        <translation>ئەللىك ئالتە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="389"/>
        <source>Fifty-eight</source>
        <translation>ئەللىك سەككىز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="391"/>
        <source>Sixty</source>
        <translation>ئاتمىش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="392"/>
        <source>Sixty-two</source>
        <translation>ئاتمىش ئىككى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="393"/>
        <source>Sixty-four</source>
        <translation>ئاتمىش تۆت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="394"/>
        <source>Sixty-six</source>
        <translation>ئاتمىش ئالتە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="395"/>
        <source>Sixty-eight</source>
        <translation>ئاتمىش سەككىز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="397"/>
        <source>Seventy</source>
        <translation>يەتمىش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="398"/>
        <source>Seventy-two</source>
        <translation>يەتمىش ئىككى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="399"/>
        <source>Seventy-four</source>
        <translation>يەتمىش تۆت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="400"/>
        <source>Seventy-six</source>
        <translation>يەتمىش ئالتە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="401"/>
        <source>Seventy-eight</source>
        <translation>يەتمىش سەككىز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="403"/>
        <source>Eighty</source>
        <translation>سەكسەن</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="404"/>
        <source>Eighty-two</source>
        <translation>سەكسەن ئىككى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="405"/>
        <source>Eighty-four</source>
        <translation>سەكسەن تۆت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="406"/>
        <source>Eighty-six</source>
        <translation>سەكسەن ئالتە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="407"/>
        <source>Eighty-eight</source>
        <translation>سەكسەن سەككىز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="409"/>
        <source>Ninety</source>
        <translation>توقسان</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="410"/>
        <source>Ninety-two</source>
        <translation>توقسان ئىككى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="411"/>
        <source>Ninety-four</source>
        <translation>توقسان تۆت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="412"/>
        <source>Ninety-six</source>
        <translation>توقسان ئالتە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="413"/>
        <source>Ninety-eight</source>
        <translation>توقسان سەككىز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="415"/>
        <source>One hundred</source>
        <translation>يۈز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="416"/>
        <source>One hundred and Two</source>
        <translation>يۈز ئىككى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="417"/>
        <source>One hundred and four</source>
        <translation>يۈز تۆت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="418"/>
        <source>One hundred and Six</source>
        <translation>يۈز ئالتە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="419"/>
        <source>One hundred and Eight</source>
        <translation>يۈز سەككىز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="421"/>
        <source>One hundred and Ten</source>
        <translation>يۈز ئون</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="422"/>
        <source>One hundred and Twelve</source>
        <translation>يۈز ئون ئىككى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="423"/>
        <source>One hundred and Fourteen</source>
        <translation>يۈز ئون تۆت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="424"/>
        <source>One hundred and Sixteen</source>
        <translation>يۈز ئون ئالتە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="425"/>
        <source>One hundred and Eighteen</source>
        <translation>يۈز ئون سەككىز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="427"/>
        <source>One hundred and Twenty</source>
        <translation>يۈز يىگىرمە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="428"/>
        <source>One hundred and Twenty-two</source>
        <translation>بىر يۈز يىگىرمە ئىككى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="429"/>
        <source>One hundred and Twenty-four</source>
        <translation>يۈز يىگىرمە تۆت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="430"/>
        <source>One hundred and Twenty-six</source>
        <translation>يۈز يىگىرمە ئالتە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceCpu.cpp" line="431"/>
        <source>One hundred and Twenty-eight</source>
        <translation>بىر يۈز يىگىرمە سەككىز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="43"/>
        <source>GDDR capacity</source>
        <translation>GDDR سىغىمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="44"/>
        <source>GPU vendor</source>
        <translation>GPU ساتقۇچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="45"/>
        <source>GPU type</source>
        <translation>GPU تىپى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="46"/>
        <source>EGL version</source>
        <translation>EGL نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="47"/>
        <source>EGL client APIs</source>
        <translation>EGL خېرىدار API لىرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="48"/>
        <source>GL version</source>
        <translation>GL نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceGpu.cpp" line="49"/>
        <source>GLSL version</source>
        <translation>GLSL نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInfo.cpp" line="69"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="183"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="204"/>
        <source>Unknown</source>
        <translation>نامەلۇم</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="201"/>
        <source>MSC</source>
        <translation>MSC</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceInput.cpp" line="203"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="371"/>
        <source>Hardware Class</source>
        <translation>قاتتىق دېتال سىنىپى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="698"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="725"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="752"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="787"/>
        <source>CPU</source>
        <translation>CPU</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="698"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="725"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="752"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="787"/>
        <source>No CPU found</source>
        <translation>CPU تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="699"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="726"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="753"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="788"/>
        <source>Motherboard</source>
        <translation>باش تاختا</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="699"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="726"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="753"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="788"/>
        <source>No motherboard found</source>
        <translation>باش تاختا تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="700"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="727"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="754"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="789"/>
        <source>Memory</source>
        <translation>ساقلىغۇ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="700"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="727"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="754"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="789"/>
        <source>No memory found</source>
        <translation>ئىچكى ساقلىغۇچ تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="701"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="728"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="755"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="790"/>
        <source>Storage</source>
        <translation>ساقلاش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="701"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="728"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="755"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="790"/>
        <source>No disk found</source>
        <translation>دىسكا تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="702"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="729"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="756"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="791"/>
        <source>Display Adapter</source>
        <translation>كۆرسەتكۈچ ماسلاشتۇرغۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="702"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="729"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="756"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="791"/>
        <source>No GPU found</source>
        <translation>GPU تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="703"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="730"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="757"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="792"/>
        <source>Monitor</source>
        <translation>نازارەتچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="703"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="730"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="757"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="792"/>
        <source>No monitor found</source>
        <translation>نازارەتچى تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="704"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="731"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="758"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="793"/>
        <source>Network Adapter</source>
        <translation>تور ماسلاشتۇرغۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="704"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="731"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="758"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="793"/>
        <source>No network adapter found</source>
        <translation>تور ماسلاشتۇرغۇچ تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="705"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="732"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="759"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="794"/>
        <source>Sound Adapter</source>
        <translation>ئاۋاز ماسلاشتۇرغۇچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="705"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="732"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="759"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="794"/>
        <source>No audio device found</source>
        <translation>ئاۋاز ئۈسكۈنىسى تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="706"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="733"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="760"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="795"/>
        <source>Bluetooth</source>
        <translation>كۆك چىش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="706"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="733"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="760"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="795"/>
        <source>No Bluetooth device found</source>
        <translation>كۆك چىش ئۈسكۈنىسى تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="707"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="734"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="761"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="796"/>
        <source>Other PCI Devices</source>
        <translation>باشقا PCI ئۈسكۈنىلىرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="707"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="734"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="761"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="796"/>
        <source>No other PCI devices found</source>
        <translation>باشقا PCI ئۈسكۈنىلىرى تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="708"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="735"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="762"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="797"/>
        <source>Power</source>
        <translation>توك</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="708"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="735"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="762"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="797"/>
        <source>No battery found</source>
        <translation>باتارېيە تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="709"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="736"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="763"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="798"/>
        <source>Keyboard</source>
        <translation>كۇنۇپكا تاختىسى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="709"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="736"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="763"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="798"/>
        <source>No keyboard found</source>
        <translation>ھېچقانداق كۇنۇپكا تاختىسى تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="710"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="737"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="764"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="799"/>
        <source>Mouse</source>
        <translation>چاشقىنەك</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="710"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="737"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="764"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="799"/>
        <source>No mouse found</source>
        <translation>چاشقىنەك تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="711"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="738"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="765"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="800"/>
        <source>Printer</source>
        <translation>پرىنتېر</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="711"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="738"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="765"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="800"/>
        <source>No printer found</source>
        <translation>پرىنتېر تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="712"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="739"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="766"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="801"/>
        <source>Camera</source>
        <translation>كامېرا</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="712"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="739"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="766"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="801"/>
        <source>No camera found</source>
        <translation>كامېرا تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="713"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="740"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="767"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="802"/>
        <source>CD-ROM</source>
        <translation>CD-ROM</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="713"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="740"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="767"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="802"/>
        <source>No CD-ROM found</source>
        <translation>CD-ROM تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="714"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="741"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="768"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="803"/>
        <source>Other Devices</source>
        <translation>باشقا ئۈسكۈنىلەر</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="714"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="741"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="768"/>
        <location filename="../src/DeviceManager/DeviceManager.cpp" line="803"/>
        <source>No other devices found</source>
        <translation>باشقا ئۈسكۈنىلەر تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="64"/>
        <source>Array Handle</source>
        <translation>گورۇپا تۇتقۇ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="66"/>
        <source>Form Factor</source>
        <translation>شەكىل ئامىلى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="67"/>
        <source>Set</source>
        <translation>بىكىتىش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="68"/>
        <source>Bank Locator</source>
        <translation>سېغم بەلگىلىگۈچ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="69"/>
        <source>Type Detail</source>
        <translation>تەپسىلات تىپى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="71"/>
        <source>Part Number</source>
        <translation>بۆلەك نومۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="72"/>
        <source>Rank</source>
        <translation>رەت تەرتىپى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="73"/>
        <source>Memory Technology</source>
        <translation>ئىچكى ساقلىغۇچ تېخنىكىسى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="74"/>
        <source>Memory Operating Mode Capability</source>
        <translation>ئىچكى ساقلىغۇچ مەشغۇلات ئىقتىدارى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="75"/>
        <source>Firmware Version</source>
        <translation>زاپاس نەشرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="76"/>
        <source>Module Manufacturer ID</source>
        <translation>Module Manufacturer ID</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="77"/>
        <source>Module Product ID</source>
        <translation>IDمودۇل مەھسۇلات</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="78"/>
        <source>Memory Subsystem Controller Manufacturer ID</source>
        <translation>ئىچكى ساقلىغۇچ سىستېمىسى كونتروللىغۇچ ئىشلەپچىقارغۇچى كىملىكى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="79"/>
        <source>Memory Subsystem Controller Product ID</source>
        <translation>ئىچكى ساقلىغۇچ سىستېمىسى كونتروللىغۇچ مەھسۇلات كىملىكى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="80"/>
        <source>Non-Volatile Size</source>
        <translation>تۇراقسىز چوڭلۇق</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="81"/>
        <source>Volatile Size</source>
        <translation>ئۆزگىرىشچان چوڭلۇق</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="82"/>
        <source>Cache Size</source>
        <translation>Cache Size</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMemory.cpp" line="83"/>
        <source>Logical Size</source>
        <translation>لوگىكىلىق چوڭلۇق</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="43"/>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="56"/>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="382"/>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="409"/>
        <source>inch</source>
        <translation>ئىنگىلىزچى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceMonitor.cpp" line="260"/>
        <source>Date</source>
        <translation>چېسلا</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="227"/>
        <source>ioport</source>
        <translation>ioport</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceNetwork.cpp" line="229"/>
        <source>network</source>
        <translation>تور</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="38"/>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="64"/>
        <source>battery</source>
        <translation>باتارېيە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="181"/>
        <source>native-path</source>
        <translation>يەرلىك-يول</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="182"/>
        <source>power supply</source>
        <translation>توك بىلەن تەمىنلەش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="183"/>
        <source>updated</source>
        <translation>يېڭىلاندى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="184"/>
        <source>has history</source>
        <translation>تارىخقا ئىگە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="185"/>
        <source>has statistics</source>
        <translation>ستاتىستىكىسى بار</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="186"/>
        <source>rechargeable</source>
        <translation>توك قاچىلىغىلى بولىدۇ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="187"/>
        <source>state</source>
        <translation>ھالەت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="188"/>
        <source>warning-level</source>
        <translation>ئاگاھلاندۇرۇش دەرىجىسى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="189"/>
        <source>energy</source>
        <translation>ئېنېرگىيە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="190"/>
        <source>energy-empty</source>
        <translation>ئېنېرگىيەسىز</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="191"/>
        <source>energy-full</source>
        <translation>ئېنېرگىيە تولۇق</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="192"/>
        <source>energy-full-design</source>
        <translation>ئېنېرگىيە تولۇق لايىھىلەش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="193"/>
        <source>energy-rate</source>
        <translation>ئېنېرگىيە نىسبىتى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="194"/>
        <source>voltage</source>
        <translation>توك بېسىمى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="195"/>
        <source>percentage</source>
        <translation>پىرسەنت</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="196"/>
        <source>temperature</source>
        <translation>تېمپېراتۇرا</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="197"/>
        <source>technology</source>
        <translation>تېخنىكا</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="198"/>
        <source>icon-name</source>
        <translation>سىنبەلگە نامى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="199"/>
        <source>online</source>
        <translation>توردا</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="200"/>
        <source>daemon-version</source>
        <translation>ساقلىغۇچ نەشىرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="201"/>
        <source>on-battery</source>
        <translation>باتارېيەدە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="202"/>
        <source>lid-is-closed</source>
        <translation>قاپاق تاقالغان</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="203"/>
        <source>lid-is-present</source>
        <translation>lid-is-present</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePower.cpp" line="204"/>
        <source>critical-action</source>
        <translation>زەربە-مەشغۇلات</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="116"/>
        <source>copies</source>
        <translation>نۇسخىلىرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="119"/>
        <source>job-cancel-after</source>
        <translation>خىزمەتنى ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="120"/>
        <source>job-hold-until</source>
        <translation>خىزمەتنى تۇتۇش بۆلىكى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="121"/>
        <source>job-priority</source>
        <translation>خىزمەت تەرتىپى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="123"/>
        <source>marker-change-time</source>
        <translation>marker-change-time</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="126"/>
        <source>number-up</source>
        <translation>يۇقرى-سان</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="127"/>
        <source>orientation-requested</source>
        <translation>يۆنىلىش تەلەپ قىلىنغان</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="128"/>
        <source>print-color-mode</source>
        <translation>رەڭلىك بىسىش ھالىتى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="132"/>
        <source>printer-is-accepting-jobs</source>
        <translation>پرىنتېر-قوبۇل قىلىش-خىزمەتلەر</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="133"/>
        <source>printer-is-shared</source>
        <translation>پرىنتېر ئورتاقلاشتى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="134"/>
        <source>printer-is-temporary</source>
        <translation>پرىنتېر ۋاقىتلىق</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="136"/>
        <source>printer-make-and-model</source>
        <translation>پرىنتېر ياساش ۋە مودېل</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="138"/>
        <source>printer-state-change-time</source>
        <translation>printer-state-change-time</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="139"/>
        <source>printer-state-reasons</source>
        <translation>printer-state-reasons</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="140"/>
        <source>printer-type</source>
        <translation>پرىنتېر تىپى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="141"/>
        <source>printer-uri-supported</source>
        <translation>printer-uri قوللايدۇ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DevicePrint.cpp" line="142"/>
        <source>sides</source>
        <translation>يان تەرەپ</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="179"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="200"/>
        <source>SSD</source>
        <translation>SSD</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="181"/>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="202"/>
        <source>HDD</source>
        <translation>HDD</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="374"/>
        <source>bus info</source>
        <translation>باس ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="376"/>
        <source>logicalsectorsize</source>
        <translation>لوگىكىلىق سېكتور چوڭلۇقى</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="378"/>
        <source>sectorsize</source>
        <translation>ساھە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="379"/>
        <source>guid</source>
        <translation>كۆرسەتمە</translation>
    </message>
    <message>
        <location filename="../src/DeviceManager/DeviceStorage.cpp" line="382"/>
        <source>Geometry (Logical)</source>
        <translation>گېئومېتىرىيە (لوگىكىلىق)</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="46"/>
        <location filename="../src/main.cpp" line="50"/>
        <source>Device Manager</source>
        <translation>ئۈسكۈنە باشقۇرغۇچى</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="52"/>
        <source>Device Manager is a handy tool for viewing hardware information and managing the devices.</source>
        <translation>ئۈسكۈنە باشقۇرغۇچى قاتتىق دېتال ئۇچۇرلىرىنى كۆرۈش ۋە ئۈسكۈنىلەرنى باشقۇرۇشتىكى قۇلايلىق قورال.</translation>
    </message>
</context>
<context>
    <name>TableWidget</name>
    <message>
        <location filename="../src/Widget/TableWidget.cpp" line="23"/>
        <location filename="../src/Widget/TableWidget.cpp" line="135"/>
        <source>Disable</source>
        <translation>چەكلەش</translation>
    </message>
    <message>
        <location filename="../src/Widget/TableWidget.cpp" line="24"/>
        <source>Refresh</source>
        <translation>يېڭىلاش</translation>
    </message>
    <message>
        <location filename="../src/Widget/TableWidget.cpp" line="25"/>
        <source>Export</source>
        <translation>ئېكسپورت</translation>
    </message>
    <message>
        <location filename="../src/Widget/TableWidget.cpp" line="137"/>
        <location filename="../src/Widget/TableWidget.cpp" line="167"/>
        <source>Enable</source>
        <translation>قوزغىتىش</translation>
    </message>
</context>
<context>
    <name>TextBrowser</name>
    <message>
        <location filename="../src/Widget/TextBrowser.cpp" line="19"/>
        <source>Refresh</source>
        <translation>يېڭىلاش</translation>
    </message>
    <message>
        <location filename="../src/Widget/TextBrowser.cpp" line="20"/>
        <source>Export</source>
        <translation>ئېكسپورت</translation>
    </message>
    <message>
        <location filename="../src/Widget/TextBrowser.cpp" line="21"/>
        <source>Copy</source>
        <translation>كۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/Widget/TextBrowser.cpp" line="254"/>
        <source>Disable</source>
        <translation>چەكلەش</translation>
    </message>
</context>
<context>
    <name>ThreadPool</name>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="218"/>
        <source>Loading Audio Device Info...</source>
        <translation>ئاۋاز ئۈسكۈنىسى ئۇچۇرىنى يۈكلەۋاتىدۇ ...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="221"/>
        <source>Loading BIOS Info...</source>
        <translation>BIOS ئۇچۇرىنى يۈكلەۋاتىدۇ ...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="231"/>
        <source>Loading CD-ROM Info...</source>
        <translation>CD-ROM ئۇچۇرلىرىنى يۈكلەۋاتىدۇ ...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="235"/>
        <source>Loading Bluetooth Device Info...</source>
        <translation>كۆك چىش ئۈسكۈنىسى ئۇچۇرىنى يۈكلەۋاتىدۇ ...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="236"/>
        <source>Loading Image Devices Info...</source>
        <translation>رەسىم ئۈسكۈنىلىرى ئۇچۇرىنى يۈكلەۋاتىدۇ ...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="238"/>
        <source>Loading Keyboard Info...</source>
        <translation>كۇنۇپكا تاختىسى ئۇچۇرىنى يۈكلەۋاتىدۇ ...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="244"/>
        <source>Loading Operating System Info...</source>
        <translation>مەشغۇلات سىستېمىسى ئۇچۇرىنى يۈكلەۋاتىدۇ ...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="246"/>
        <source>Loading CPU Info...</source>
        <translation>CPU ئۇچۇرىنى يۈكلەۋاتىدۇ ...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="247"/>
        <source>Loading Other Devices Info...</source>
        <translation>باشقا ئۈسكۈنىلەر ئۇچۇرىنى يۈكلەۋاتىدۇ ...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="248"/>
        <source>Loading Power Info...</source>
        <translation>يۈكلەش ئۇچۇرى ...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="249"/>
        <source>Loading Printer Info...</source>
        <translation>پرىنتېر ئۇچۇرلىرىنى يۈكلەۋاتىدۇ ...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="256"/>
        <source>Loading Monitor Info...</source>
        <translation>نازارەتچى ئۇچۇرىنى يۈكلەۋاتىدۇ ...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="257"/>
        <source>Loading Mouse Info...</source>
        <translation>مائۇس ئۇچۇرىنى يۈكلەۋاتىدۇ ...</translation>
    </message>
    <message>
        <location filename="../src/ThreadPool/ThreadPool.cpp" line="258"/>
        <source>Loading Network Adapter Info...</source>
        <translation>تور ماسلاشتۇرغۇچ ئۇچۇرىنى يۈكلەۋاتىدۇ ...</translation>
    </message>
</context>
<context>
    <name>WaitingWidget</name>
    <message>
        <location filename="../src/Page/WaitingWidget.cpp" line="15"/>
        <source>Loading...</source>
        <translation>يۈكلەۋاتىدۇ...</translation>
    </message>
</context>
</TS>